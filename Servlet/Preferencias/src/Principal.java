
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Principal
 */
@WebServlet("/Principal")
public class Principal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Principal() {
		// TODO Auto-generated constructor stub
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// PrintWriter printwriter = response.getWriter();
		// include ejemplo.jsp
		RequestDispatcher rd = request.getRequestDispatcher("ejemplo.jsp");

		if (request.getParameter("clearCookies") != null) {
			Cookie ck = new Cookie("color", "default");
			ck.setMaxAge(0);
			response.addCookie(ck);
			response.sendRedirect("http://localhost:8080/Preferencias/Principal");
		}
		if (request.getParameter("color") != null) {
			Cookie ck = new Cookie("color", request.getParameter("color"));
			response.addCookie(ck);
			response.sendRedirect("http://localhost:8080/Preferencias/Principal");
		}
		
		Cookie c[] = request.getCookies();
		String color = "default";
		if (c != null) {
			for (int i = 0; i < c.length; i++) {
				if (c[i].getName().equals("color")) {
					color = c[i].getValue();
				}
			}
		}
		if (color.equals("default")) {
			javax.servlet.ServletContext context = getServletContext();
			color = context.getInitParameter("color");
		}
		request.setAttribute("color", color);
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
