<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.Collections,java.util.Arrays,java.util.List,java.util.AbstractList,java.util.ArrayList,java.util.Iterator"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3> Random pairs : </h3>
<% 

String names = (String) session.getAttribute("names");
List<String> list = new ArrayList<String>();
list = Arrays.asList(names.split(";"));
Collections.shuffle(list);

boolean solo = true;
for (String name : list ) {
	out.println(name);
	if (!solo) {
		out.println("<br>");
	}
	// change from TRUE to FALSE and FALSE to TRUE
	solo = !solo;
}

%>
</body>
</html>