<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%


String name = (String) request.getParameter("name");
// name = name.replaceAll("[^a-zA-Z]", "");
out.println("Before adding names are:" + ((String) session.getAttribute("names")) + "<br>");
if (name != null && !name.equals("")) {
	out.println("Session ID:" + session.getId() + "<br>");
	out.println("Added " + name + "<br>"); 
	
	if (session.getAttribute("names") != null ) { 
		String names = (String) session.getAttribute("names");
		out.println("Before adding names are:" + names + "<br>"); 
		names = names + ";" + name.replaceAll("[^a-zA-Z ]", ""); 
		session.setAttribute("names", names);
		out.println("Added a name, now they are : " + names + "<br>");
	} else {
		session.setAttribute("names", name);
		response.sendRedirect("index.jsp");
	}
}

// if the user ask to reset we delete his attributes
if (request.getParameter("reset") != null ) {
	if (request.getParameter("reset").equals("y")) {
		out.println("deleting session");
		session.removeAttribute("names");
		response.sendRedirect("index.jsp");
	}
}

%>

<form>
Name:
<input type="text" name="name">
<br>
<button type="submit">Add</button>
</form>
<br>
<a Href="index.jsp?reset=y">Delete all</a>
<a href="sorteo.jsp">Sorteo!</a>
</body>
</html>