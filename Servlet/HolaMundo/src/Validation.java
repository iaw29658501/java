import java.util.regex.Pattern;

public class Validation {
	public static boolean firstname(String input) {
		return Pattern.matches("^[A-Z][a-z]{2,50}$", input);
	}

	public static boolean surname(String input) {
		return Pattern.matches("^[A-Z\\sa-z]{3,60}$", input);
	}

	public static boolean email(String input) {
		return Pattern.matches("^[A-Za-z0-9._%+-]{1,100}@[A-Za-z0-9.-]{1,50}\\.[A-Za-z]{2,4}$", input);
	}

	public static boolean date(String input) {
		return Pattern.matches("^\\d\\d/\\d\\d/\\d\\d\\d\\d$", input);
	}

	public static boolean dni(String input) {
		if (Pattern.matches("^[0-9]{8,8}[A-Za-z]$", input )) {
			String dni = input;
			String dnicheck = "TRWAGMYFPDXBNJZSQVHLCKE";
			int neededPosition = Integer.parseInt(dni.substring(0, dni.length() - 1)) % 23;
			if (dnicheck.charAt(neededPosition) == dni.charAt(dni.length()-1)) {
				return true;
			}  else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
