import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaMundo
 */

@WebServlet(description = "testing", urlPatterns = { "/HolaMundo" })
public class HolaMundo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public HolaMundo() {
		// TODO Auto-generated constructor stub
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		PrintWriter printwriter = response.getWriter();

		printwriter.println(docType);
		printwriter.println(" <form>\n" + "  1. First name:<br>\n" + "  <input type=\"text\" name=\"firstname\"><br>\n"
				+ " Surname name:<br>\n" + "  <input type=\"text\" name=\"surname\"><br>\n" + "  email <br>\n"
				+ "  <input type=\"email\" name=\"email\">\n" + "    <br> DNI:<br>\n"
				+ "  <input type=\"text\" name=\"dni\">\n" + "    <br> date of birth (dd/mm/yyyy):<br>\n"
				+ "  <input type=\"text\" name=\"date\">\n" + "<br> <button type=\"submit\">submit</button>"
				+ "</form> ");
		printwriter.println();
		// Validation.sayme();
		ArrayList<String> errores = new ArrayList<String>();
		// Check name
		if (Validation.firstname(request.getParameter("firstname"))) {
			printwriter.println("Nice name<br>");
		} else {
			errores.add("<br>Invalid name please only use letters<br>");
		}

		if (Validation.surname(request.getParameter("surname"))) {
			printwriter.println("Nice surname<br>");
		} else {
			errores.add("<br>Surname invalid<br>");
		}

		if (Validation.email(request.getParameter("email"))) {
			printwriter.println("Nice email<br>");
		} else {
			errores.add("<br>Invalid Email<br>");
		}

		if (Validation.date(request.getParameter("date"))) {
			printwriter.println("Nice day of birth<br>");
		} else {
			errores.add("<br>Invalid birth day<br>");
		}
		

		// check dni
		if (Validation.dni(request.getParameter("dni"))) {
			printwriter.println("Nice dni<br>");
		} else {
			errores.add("<br>Invalid dni<br>");
		}

		// output errors
		for (String s : errores) {
			printwriter.println(s);
		}

	}

}
