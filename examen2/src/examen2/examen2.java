/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author gmartinez
 */
public class examen2 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		File fichero = new File("Naus_Dades.dat");
		File indexFile = new File("Naus_Dades_Index.dat");
		ArrayList<Naus_Dades> arrayListDeNaus = new ArrayList<Naus_Dades>();
		LocalDateTime today;
		int longString;
		StringBuffer buffer = null;

		today = LocalDateTime.now();

		// Per a passar de String --> char[]: es fa servir .toCharArray() sobre el
		// string.
		arrayListDeNaus.add(new Naus_Dades("Agamemnon", "destructor", "Omega".toCharArray(), today,
				"Enviat a la flota de Babylon 5"));
		arrayListDeNaus.add(new Naus_Dades("Achilles", "destructor", "Omega".toCharArray(), today,
				"Enviat a la flota de Babylon 5"));
		arrayListDeNaus.add(
				new Naus_Dades("Cortez", "explorador", "Explorer".toCharArray(), today, "Enviat a l'espai profund"));
		arrayListDeNaus
				.add(new Naus_Dades("Orion", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Mart"));
		arrayListDeNaus
				.add(new Naus_Dades("Nimrod", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Mart"));
		System.out.println("---------GRAVEM LES NAUS----------");

		try {
			RandomAccessFile fitxerEmmagatzematge = new RandomAccessFile(fichero, "rw");
			RandomAccessFile indexFileR = new RandomAccessFile(indexFile, "rw");
			// PrintWriter writer = new PrintWriter(indexFile, "UTF-8");
			// RandomAccessFile no sap escriure i llegir objectes, ni String, ni
			// LocalDateTime,...
			int lastPointer = 0;
			int tamany;
			for (Naus_Dades nauTmp : arrayListDeNaus) {
				// System.out.println(nauTmp.getDataConstruccio());

				// Gravem el nom (String nom).
				longString = nauTmp.getNom().length();
				fitxerEmmagatzematge.writeInt(longString);
				fitxerEmmagatzematge.writeChars(nauTmp.getNom());

				// Gravem el tipus (String tipus).
				longString = nauTmp.getTipus().length();
				fitxerEmmagatzematge.writeInt(longString);
				fitxerEmmagatzematge.writeChars(nauTmp.getTipus());

				// Gravem el model (char model[50]).
				// Per a passar de char[] --> String: String.valueOf(X) on X és de tipus char[].
				buffer = new StringBuffer(String.valueOf(nauTmp.getModel()));
				buffer.setLength(50);
				fitxerEmmagatzematge.writeChars(buffer.toString());

				// Gravem la dataConstruccio (LocalDateTime dataConstruccio).
				fitxerEmmagatzematge.writeChars(String.valueOf(nauTmp.getDataConstruccio().toString()));

				// Gravem la descripció (String descripcio).
				longString = nauTmp.getDescripcio().length();

				fitxerEmmagatzematge.writeInt(longString);
				fitxerEmmagatzematge.writeChars(nauTmp.getDescripcio());
				// Ejercicio aqui
				// Calculare el tamanho haciendo el delta del pointer (algo mas inteligente)
				tamany = (int) (fitxerEmmagatzematge.getFilePointer() - lastPointer);
				lastPointer = (int) fitxerEmmagatzematge.getFilePointer();
				System.out.println(nauTmp.getNom() + " tamany : " + tamany);
				// Me gustaria hacer el writeInt como siempre pero escribindo como Strings me funcionava mejor (despues hago un parse para int mas adelante)
				// lo bueno desta version es que otros programas en otras lenguas tambien pueden ler y hasta un humano lo puede
				indexFileR.writeUTF(tamany + "\n");
			}

			String stringTmp;
			fitxerEmmagatzematge.close();
			indexFileR.close();
			fitxerEmmagatzematge = new RandomAccessFile(fichero, "r");

			while (fitxerEmmagatzematge.getFilePointer() != fitxerEmmagatzematge.length()) {
				System.out.println("----------------");

				// LLegim el nom (String nom).
				stringTmp = "";
				longString = fitxerEmmagatzematge.readInt();
				System.out.println("Nom.length(): " + longString);
				for (int i = 0; i < longString; i++) {
					// System.out.println("Nom: " + fitxerEmmagatzematge.readChar());
					stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
				}
				System.out.println("Nom: " + stringTmp);

				// LLegim el tipus (String tipus).
				stringTmp = "";
				longString = fitxerEmmagatzematge.readInt();
				System.out.println("tipus.length(): " + longString);
				for (int i = 0; i < longString; i++) {
					// System.out.println("Tipus: " + fitxerEmmagatzematge.readChar());
					stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
				}
				System.out.println("Tipus: " + stringTmp);

				// LLegim el model (char model[50]).
				stringTmp = "";
				for (int i = 0; i < 50; i++) {
					// System.out.println("Model: " + fitxerEmmagatzematge.readChar());
					stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
				}
				System.out.println("Model: " + stringTmp);

				// LLegim la dataConstruccio (LocalDateTime dataConstruccio).
				stringTmp = "";
				for (int i = 0; i < 23; i++) {
					// System.out.println("Data construcció: " + fitxerEmmagatzematge.readChar());
					stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
				}
				System.out.println("Data construcció: " + stringTmp);

				// LLegim la descripció (String descripcio).
				stringTmp = "";
				longString = fitxerEmmagatzematge.readInt();
				System.out.println("descripcio.length(): " + longString);
				for (int i = 0; i < longString; i++) {
					// System.out.println("Descripció: " + fitxerEmmagatzematge.readChar());
					stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
				}
				System.out.println("Descripció: " + stringTmp);
			}

		} catch (FileNotFoundException ex) {
			ex.getMessage();
		} catch (IOException ex) {
			ex.getMessage();
		}
		try {
			RandomAccessFile fitxerEmmagatzematge = new RandomAccessFile(fichero, "r");
			RandomAccessFile indexFileRAF = new RandomAccessFile(indexFile, "r");
			llegirUnaNau(fitxerEmmagatzematge, indexFileRAF);
			llegirLaDescripcioDUnaNau(fitxerEmmagatzematge, indexFileRAF);
		} catch (FileNotFoundException ex) {
			ex.getMessage();
		}
	}

	public static void llegirUnaNau(RandomAccessFile fitxerEmmagatzematge, RandomAccessFile fitxerIndex) {
		System.out.println("-------RECUPEREM I PRINTEM 1 de LES 5 NAUS -------");
		System.out.println("Selecciona 1 de les 5 naus (1-5) :");
		Scanner in = new Scanner(System.in);
		int position = in.nextInt();

		// lets calculate 'where does this NAU starts ?'
		try {
			System.out.println("Input:" + position);
			String nextUTF;
			int startPoint = 0;
			for (int i = 1; i < position; i++) {
				nextUTF = fitxerIndex.readUTF().replace("\n", "").replace("\r", "");
				startPoint += Integer.parseInt(nextUTF);
			}
			String tamany = fitxerIndex.readUTF();
			System.out.println("Starts at: " + startPoint + " tamaño: " + tamany);
			fitxerEmmagatzematge.seek(startPoint);
			// este codigo aseguir es el tuyo
			String stringTmp = "";
			int longString = fitxerEmmagatzematge.readInt();
			System.out.println("Nom.length(): " + longString);
			for (int i = 0; i < longString; i++) {
				// System.out.println("Nom: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Nom: " + stringTmp);

			// LLegim el tipus (String tipus).
			stringTmp = "";
			longString = fitxerEmmagatzematge.readInt();
			System.out.println("tipus.length(): " + longString);
			for (int i = 0; i < longString; i++) {
				// System.out.println("Tipus: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Tipus: " + stringTmp);

			// LLegim el model (char model[50]).
			stringTmp = "";
			for (int i = 0; i < 50; i++) {
				// System.out.println("Model: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Model: " + stringTmp);

			// LLegim la dataConstruccio (LocalDateTime dataConstruccio).
			stringTmp = "";
			for (int i = 0; i < 23; i++) {
				// System.out.println("Data construcció: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Data construcció: " + stringTmp);

			// LLegim la descripció (String descripcio).
			stringTmp = "";
			longString = fitxerEmmagatzematge.readInt();
			System.out.println("descripcio.length(): " + longString);
			for (int i = 0; i < longString; i++) {
				// System.out.println("Descripció: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Descripció: " + stringTmp);

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void llegirLaDescripcioDUnaNau(RandomAccessFile fitxerEmmagatzematge, RandomAccessFile fitxerIndex) {
		System.out.println("-------RECUPEREM I PRINTEM LA DESCRIPCIO DE 1 de LES 5 NAUS -------");
		System.out.println("Selecciona 1 de les 5 naus (1-5) :");
		Scanner in = new Scanner(System.in);
		int position = in.nextInt();

		// lets calculate 'where does this NAU starts ?'
		try {
			String nextUTF;
			//variable para calcular el punta de inicio deste objecto
			int startPoint = 0;
			for (int i = 1; i < position; i++) {
				nextUTF = fitxerIndex.readUTF().replace("\n", "").replace("\r", "");
				startPoint += Integer.parseInt(nextUTF);
			}
			// tamaño del objecto
			String tamany = fitxerIndex.readUTF();
			fitxerEmmagatzematge.seek(startPoint);
			// este codigo aseguir es el tuyo
			String stringTmp = "";
			int longString = fitxerEmmagatzematge.readInt();
			for (int i = 0; i < longString; i++) {
				// System.out.println("Nom: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}

			// LLegim el tipus (String tipus).
			stringTmp = "";
			longString = fitxerEmmagatzematge.readInt();
			for (int i = 0; i < longString; i++) {
				// System.out.println("Tipus: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}

			// LLegim el model (char model[50]).
			stringTmp = "";
			for (int i = 0; i < 50; i++) {
				// System.out.println("Model: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}

			// LLegim la dataConstruccio (LocalDateTime dataConstruccio).
			stringTmp = "";
			for (int i = 0; i < 23; i++) {
				// System.out.println("Data construcció: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}

			// LLegim la descripció (String descripcio).
			stringTmp = "";
			longString = fitxerEmmagatzematge.readInt();
			for (int i = 0; i < longString; i++) {
				// System.out.println("Descripció: " + fitxerEmmagatzematge.readChar());
				stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
			}
			System.out.println("Descripció: " + stringTmp);

		} catch (Exception e) {
			e.getMessage();
		}
	}
}
