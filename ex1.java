package practicas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO linkedlist i l'altre amb arrayList
		LinkedList ll = new LinkedList();
		List lista = new ArrayList();
		
		long tempsInicial = System.nanoTime();
		for (int i = 0; i < 10000; i++) {
			lista.add(i);
		}
		long tempsFinal = System.nanoTime();
		System.out.println("ArrayList a tardado " + ( tempsFinal - tempsInicial ));
		
		
		long tempsInicial2 = System.nanoTime();
		for (int i = 0; i < 10000; i++) {
			ll.add(i);
		}
		long tempsFinal2 = System.nanoTime();
		System.out.println("LinkedList a tardado " + (tempsFinal2 - tempsInicial2));

		tempsInicial  = System.nanoTime();
		lista.add(0, 007);
		tempsFinal  = System.nanoTime();
		System.out.println("ArrayList a tardado " + ( tempsFinal - tempsInicial ));
		
		tempsInicial  = System.nanoTime();
		ll.addFirst(-1);
		tempsFinal  = System.nanoTime();
		System.out.println("LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		
	}

}

