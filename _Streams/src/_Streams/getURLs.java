package _Streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class getURLs {
	
	private static final String href="(<.* href=\")([^\"]+)(\")";

	public static void main(String[] args) {

		URL url;

		try {
			// get URL content

			String a = "http://www.escoladeltreball.org";
			url = new URL(a);
			URLConnection conn = url.openConnection();

			// open the stream and put it into BufferedReader
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String inputLine;
			String html = "";
			while ((inputLine = br.readLine()) != null) {
					html+=inputLine + "\n";
			}
			
			Pattern p = Pattern.compile(href);
			Matcher m = p.matcher(html);
			int i =0;
			
			File urls = new File("urls.txt");
			FileWriter fw = new FileWriter(urls);		
			
			while (m.find()) {			
				fw.write(m.group(2) + "\n");
			}
			fw.close();
			br.close();
			System.out.println("Done with urls");

			Pattern pp = Pattern.compile("(<p([^>]*)>)([^<]*)(</p>)");
			Matcher mp = pp.matcher(html);
			
			File ps = new File("paragraphs.txt");
			FileWriter fwps = new FileWriter(ps);		
			
			while (mp.find()) {			
				fwps.write(mp.group(3) + "\n");
			}
			fwps.close();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
