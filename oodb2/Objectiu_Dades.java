/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_2019_2020_DAW;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author gmartinez
 */
public class Objectiu_Dades {
    int id;                     //Clau primaria. Es crea automàticament pel sistema i és intocable.
    String nom;
    boolean actiu;              //TRUE si està actiu i es pot fer servir per afegir-lo a alguna ruta.
    LocalDate dataCreacio;
    LocalDate dataModificacio;

    
    public Objectiu_Dades(int id, String nom, boolean actiu, LocalDate dataCreacio, LocalDate dataModificacio) {
        this.id = id;
        this.nom = nom;
        this.actiu = actiu;
        this.dataCreacio = dataCreacio;
        this.dataModificacio = dataModificacio;
    }

    
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public boolean isActiu() {
        return actiu;
    }

    public LocalDate getDataCreacio() {
        return dataCreacio;
    }

    public LocalDate getDataModificacio() {
        return dataModificacio;
    }

    
    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setActiu(boolean actiu) {
        this.actiu = actiu;
    }

    public void setDataCreacio(LocalDate dataCreacio) {
        this.dataCreacio = dataCreacio;
    }

    public void setDataModificacio(LocalDate dataModificacio) {
        this.dataModificacio = dataModificacio;
    }

    
    
    
    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy");
        
        return "Objectiu " + id + ":" + 
                "\n   nom = " + nom +
                "\n   actiu = " + actiu +
                "\n   dataCreacio = " + dataCreacio.format(formatter) +
                "\n   dataModificació = " + dataModificacio.format(formatter);
    }
    
}
