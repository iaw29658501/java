package ejerciciosSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*ordreInsercio();
		HashSet<Integer> hs = new HashSet<Integer>();
		LinkedHashSet<Integer> lhs = new LinkedHashSet<Integer>();
		TreeSet<Integer> ts = new TreeSet<Integer>();
		System.out.println("Examinando distincion de tiempo");
		insertElements(hs);
		insertElements(lhs);
		insertElements(ts);*/
		// Ejercficio 3
		HashSet<Integer> hsa = new HashSet<Integer>();
		HashSet<Integer> hsb = new HashSet<Integer>();
		hsa.add(1);
		hsa.add(2);
		hsb.add(2);
	//	hsb.add(3);
		System.out.println(union(hsa,hsb).toString());
		System.out.println(inter(hsa,hsb).toString());
		System.out.println(resta(hsa,hsb).toString());
		System.out.println(isSubconjunt(hsa,hsb));
		System.out.println(xor(hsa,hsb));
	}

	public static void ordreInsercio() {
		List<String> e = Arrays.asList("groc", "blau", "verd", "vermell");
		HashSet<String> hs = new HashSet<String>();
		LinkedHashSet<String> lhs = new LinkedHashSet<String>();
		TreeSet<String> ts = new TreeSet<String>();
		hs.addAll(e);
		lhs.addAll(e);
		ts.addAll(e);
		System.out.println(hs.toString());
		System.out.println(lhs.toString());
		System.out.println(ts.toString());
	}

	public static void insertElements(Set<Integer> s) {
		long tempsInicial = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			s.add(i);
		}
		long tempsFinal = System.nanoTime();
		System.out.println("Ha tardado  " + (tempsFinal - tempsInicial));
	}
	// Exercicio 3.1
	public static <T> Set<T> union(Set<T> a, Set<T> b) {
		HashSet<T> c = new HashSet<T>(a);
		c.addAll(b);
		return c;
	}
	
	public static <T> Set<T> inter(Set<T> a, Set<T> b) {
		HashSet<T> c = new HashSet<T>(a);
		c.retainAll(b);
		return c;
	}
	
	public static <T> Set<T> resta(Set<T> a, Set<T> b) {
		HashSet<T> c = new HashSet<T>(a);
		c.removeAll(b);
		return c;
	}
	
	public static <T> boolean isSubconjunt(Set<T> a, Set<T> b) {
		return a.containsAll(b);
	}	
	
	public static <T> Set<T> xor(Set<T> a, Set<T> b) {
		return resta(union(a,b),inter(a, b));
	}	
	
	

}
