package ejerciciosSet;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;

public class paraulesDiferents {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(comparation("esto no es esto esto solo es otra cosa")[0].toString());
	}

	public static ArrayList[] comparation(String words) {
		String[] lwords = words.split("\\s+");
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> dupList = new ArrayList<String>();
		for (int i = 1; i < lwords.length; i++) {

			if (!(list.contains(lwords[i]))) {
				list.add(lwords[i]);
			} else {
				dupList.add(lwords[i]);
			}
		}
		list.removeAll(dupList);
		ArrayList[] ret = { list, dupList};
		return ret;
	}

}
