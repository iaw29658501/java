import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class contador {

	private static BufferedReader buffer;

	public static void main(String[] args) {
		List<String> files = new ArrayList<String>();
		files.add("/tmp/file1.txt");
		files.add("/tmp/file2.txt");
		String linea;
		FileReader r = null;
		File filetype = null;
		Map<Character, Integer> map = null;
		for (String file : files) {
			System.out.println("Reading file " + file);
			int nchars = 0;
			int nlines = 0;
			try {
				map = new HashMap<Character, Integer>();
				filetype = new File(file);
				r = new FileReader(filetype);
				buffer = new BufferedReader(r);
				while ((linea = buffer.readLine()) != null) {
					nlines++;
					nchars += linea.length();
					for ( int i = 0; i<linea.length(); i++ ) {
						Character letra = linea.charAt(i);
						if (map.containsKey(letra)) {
							map.put( letra, map.get(letra) + 1);
						} else {
							map.put(letra, 1);
						}
					}
				}
				File filex = new File(filetype.getParent() + "/counter" + filetype.getName());
				// el segundo parametro 'true' es para el append
				FileWriter fw1 = new FileWriter(filex);
				try {
					PrintWriter pw1 = new PrintWriter(fw1, true);
					for ( Map.Entry<Character, Integer> entry : map.entrySet()) {
						Double porcentage = ((double) entry.getValue()/ (double) nchars) * 100;
						pw1.println(entry.getKey() + " aparecio " + entry.getValue() + " veces; porcentage = " + porcentage + "%");
					}
					pw1.close();
				} catch (Exception e) {
					System.out.println(e.getMessage());

				} finally {
					fw1.close();
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} 
			System.out.println(filetype.getName() + " lineas: " + nlines + " chars: " + nchars);

		}

	}

}
