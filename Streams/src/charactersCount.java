import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class charactersCount {
	public static void main(String[] args) {
		File file = new File("/tmp/file1.txt");
		String line = null;
		Map<Character, Integer> charCount = new HashMap();
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			int nchars =0;
			while ( ( line=br.readLine() ) != null) {
				nchars += line.length();
				for ( int i = 0; i < line.length(); i++ ) {
					char character = line.charAt(i);
					if ( charCount.containsKey(character) ) {
						charCount.put(character, (charCount.get(character) + 1)  );
					} else {
						charCount.put(character,1);
					}
				}
				
			}
			System.out.println(charCount.toString());
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
