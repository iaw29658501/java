import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class getLinks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		URL url = new URL("http://escoladeltreball.org");
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String body = IOUtils.toString(in, encoding);
		Matcher m = Pattern.compile("s/.*href=\"\\([^\"]*\\).*/\\1/p").matcher(body);
		List<Integer> pos = new ArrayList<Integer>();
		while (m.find()) {
			pos.add(m.start());
		}
		System.out.println(pos);
	}

}
