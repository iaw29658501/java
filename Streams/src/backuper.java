import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class backuper {

	public static void main(String[] args) {
		File backup = new File("/tmp/backup0");

		// cambiare el nombre del directorio de backup hasta que
		// un directorio con este nombre no exista
		int i = 0;
		while (backup.exists()) {
			i++;
			System.out.println("intentando " + i);
			backup = new File("/tmp/backup" + i);
		}
		backup.mkdir();

		File copyme = new File("needinput");
		do {
			Scanner reader = new Scanner(System.in); // Reading from System.in
			System.out.println("De cual directoria quiere hacer un backup?");
			String path = reader.nextLine();
			copyme = new File(path);
		} while (!copyme.isDirectory() && !copyme.getName().equals("needinput"));
		try {
			copyThis(copyme, backup);
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
		}

	}

	public static void copyThis(File src, File dest) throws IOException {
		String files[] = src.list();
		if (src.isDirectory()) {
			for (String file : files) {
				// recursive copy
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
			//	System.out.println("PATH " +  + " END PATH");
				dest.mkdirs();
				copyThis(srcFile, destFile);
			}
		} else if (src.isFile()) {
			// if is a file then lets copy it to the dest
			if (!src.canRead()) {
				System.out.println("WARNING no se puede leer " + src);
			} else {
				
				InputStream in = new FileInputStream(src);
				OutputStream out = new FileOutputStream(dest);
				
				
				byte[] buffer = new byte[1024];

				int length;
				// copy the file content in bytes
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}

				in.close();
				out.close();
				System.out.println("File copied from " + src + " to " + dest);
			}
		}
	}
}
