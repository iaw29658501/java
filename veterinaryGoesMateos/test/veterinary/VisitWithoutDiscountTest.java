package veterinary;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class VisitWithoutDiscountTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testCalculateCost() {
		char parteDelDia = 'M';
		VisitWithoutDiscount visit = new VisitWithoutDiscount("22/01/2019", parteDelDia);
		visit.setGrossPrice(100);
		assertEquals(visit.calculateCost(), 121, 0.1);
	}

	@Test
	public void testCalculateCostJuliol() {
		char parteDelDia = 'M';
		VisitWithoutDiscount visit = new VisitWithoutDiscount("22/07/2019", parteDelDia);
		visit.setGrossPrice(100);
		assertEquals(visit.calculateCost(), 121, 0.1);
	}

	@Test
	public void testCalculateCostAgosto() {
		char parteDelDia = 'M';
		VisitWithoutDiscount visit = new VisitWithoutDiscount("22/08/2019", parteDelDia);
		visit.setGrossPrice(100);
		assertEquals(visit.calculateCost(), 121, 0.1);
	}

	@Test
	public void testCalculateCostNormal() {
		char parteDelDia = 'M';
		VisitWithoutDiscount visit = new VisitWithoutDiscount("22/01/2019", parteDelDia);
		visit.setGrossPrice(100);
		assertEquals(visit.calculateCost(), 121, 0.1);
	}

}
