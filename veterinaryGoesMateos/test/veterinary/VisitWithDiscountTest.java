package veterinary;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class VisitWithDiscountTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}	

	@Test
	public void testCalculateCost() {
		char parteDelDia = 'M';
		VisitWithDiscount visit = new VisitWithDiscount("22/01/2019", parteDelDia);
		visit.setGrossPrice(100);
		double preu = (100 - 10) * 1.21;
		assertEquals(visit.calculateCost(), preu, 0.05);
	}

	@Test
	public void testCalculateCostJuliol() {
		char parteDelDia = 'M';
		VisitWithDiscount visit = new VisitWithDiscount("22/07/2019", parteDelDia);
		visit.setGrossPrice(100);
		double preu = (100 - 15) * 1.21;
		assertEquals(visit.calculateCost(), preu, 0.05);
	}

	@Test
	public void testCalculateCostAgosto() {
		char parteDelDia = 'M';
		VisitWithDiscount visit = new VisitWithDiscount("22/08/2019", parteDelDia);
		visit.setGrossPrice(100);
		double preu = (100 - 15) * 1.21;
		assertEquals(visit.calculateCost(), preu, 0.05);
	}

	@Test
	public void testCalculateCostNormal() {
		char parteDelDia = 'M';
		VisitWithDiscount visit = new VisitWithDiscount("22/09/2019", parteDelDia);
		visit.setGrossPrice(100);
		double preu = (100 - 10) * 1.21;
		assertEquals(visit.calculateCost(), preu, 0.05);
	}
}
