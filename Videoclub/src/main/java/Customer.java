

import java.util.Enumeration;
import java.util.Vector;

class Customer {
	private String name;
	private Vector<Rental> rentals = new Vector<Rental>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental arg) {
		this.rentals.addElement(arg);
	}

	public String getName() {
		return this.name;
	}
	
	public String statement() {

		Enumeration<Rental> rentals = this.rentals.elements();
		String result = "Rental Record for " + getName() + "\n";
		while (rentals.hasMoreElements()) {
			Rental each = (Rental) rentals.nextElement();
			// add frequent renter points
			// show figures for this rental
			result += "\t" + each.getMovie().getTitle() + "\t" + String.valueOf(each.getCharge()) + "\n";
		}
		// add footer lines
		result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
		result += "You earned " + String.valueOf(getTotalFrequentRentalPoints()) + " frequent renter points";
		return result;
	}

	private int getTotalFrequentRentalPoints() {
		int result = 0;
		Enumeration<Rental> rentalsElements = rentals.elements();
		while (rentalsElements.hasMoreElements()) {
			Rental each = (Rental) rentalsElements.nextElement();
			result += each.getFrequentRenterPoints();
		}
		return result;			
	}

	private double getTotalCharge() {
		double result = 0;
		Enumeration<Rental> rentalsElements = rentals.elements();
		while (rentalsElements.hasMoreElements()) {
			Rental each = (Rental) rentalsElements.nextElement();
			result += each.getCharge();
		}
			return result;
	}

	private double amountOf(Rental aRental) {
		return aRental.getCharge();
	}
	
}
