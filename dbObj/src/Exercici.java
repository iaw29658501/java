import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;

import java.util.Scanner;


public class Exercici {
    // EXERCICIOS 3 y 4
    public static void ex3y4() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {

            // EXERCICIO 3
            Producte[] productes = {new Producte("producte_1", 1337), new Producte("producte_2", 54),
                    new Producte("producte_3", 666)};

            for (int i = 0; i < productes.length; i++) {
                db.store(productes[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
                // encarrecs associats).
            }

            Client[] clients = {new Client("client_1", "Adreça1", "e−mail1@domini.com", "+34911112211"),
                    new Client("client_1", "Adreça2", "e−mail2@domini.com", "+34922223322")};

            clients[0].addComanda(new Encarrec("patates", 27));
            clients[0].addComanda(new Encarrec("ratoli", 100));
            clients[1].addComanda(new Encarrec("Impressora", 1));
            clients[1].addComanda(new Encarrec("Toner Impressora", 4));

            for (int i = 0; i < clients.length; i++) {
                db.store(clients[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
                // encarrecs associats).
            }

        } finally {
            db.close();
        }
    }

    // EXERCICIO 5
    public static void listProducts() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            // SQL "WHERE"
            Predicate p = new Predicate<Producte>() {
                @Override
                public boolean match(Producte c) {
                    return true;
                }
            };
            ObjectSet<Producte> result = db.query(p);

            while (result.hasNext()) {
                Producte prod = result.next();
                System.out.println("Producte: " + prod.toString());
            }
        } finally {
            db.close();
            System.out.println("veureClients1(): FINAL");
            System.out.println("--------");
        }
    }

    /*
    public static Producte getProd(String prodnom){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            Predicate p = new Predicate<Producte>() {
                @Override
                public boolean match(Producte c) {
                    boolean b = c.getProducteNom().equalsIgnoreCase(prodnom);
                    return b;
                }
            };

            ObjectSet<Producte> result = db.query(p);
            if (result.hasNext()) {
                Producte e = result.next();
                return e;
            }
        } finally {
            db.close();
        }
        return null;
    }
    public static Client getClient(String clienom){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            Predicate p = new Predicate<Client>() {
                @Override
                public boolean match(Client c) {
                    boolean b = c.getNom().equalsIgnoreCase(clienom);
                    return b;
                }
            };

            ObjectSet<Client> result = db.query(p);
            if (result.hasNext()) {
                Client e = result.next();
                return e;
            }
        } finally {
            db.close();
        }
        return null;
    }
    */
    // EXERCICIO 6
    // input: nombre del cliente , nombre del producto y cuantidad
    private static void afegirEncarrec(String nom, String prod, int quant) {
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Client.class).cascadeOnUpdate(true);
        ObjectContainer db = Db4oEmbedded.openFile(config, "baseDeDades/BDOOClients.db4o");

        //Cercar clients a la BDOO i obtenir−los a memòria com a objectes del programa
        try {
            Predicate p = new Predicate<Client>() {
                @Override
                public boolean match(Client c) {
                    return c.getNom().equalsIgnoreCase(nom);
                }
            };
            Predicate predprod = new Predicate<Producte>() {
                @Override
                public boolean match(Producte pr) {
                    return pr.getProducteNom().equalsIgnoreCase(prod);
                }
            };

            ObjectSet<Client> clients = db.query(p);
            ObjectSet<Producte> productes = db.query(predprod);
            if (clients.size() != 1 || productes.size() != 1) {
                System.out.println("No es pot modificar, no es unico! Clients.size:" + clients.size() + " prod.size" + productes.size() );
            } else {
                Encarrec ne = new Encarrec(productes.next(), quant);
                Client c = clients.next();
                c.addComanda(ne);
                db.store(c);
            }
        } finally {
            db.close();
            System.out.println("AfegirEncarrec(): FINAL");
            System.out.println("--------");
        }
    }


    public static void main(String[] args) {
         // ex3y4();
         // ex5 :
         // listProducts();
         afegirEncarrec("Client1", "producte_1", 10);
         afegirEncarrec("Client2", "producte_2", 1000);
         afegirEncarrec("Client1", "producte_3", 1000);
         afegirEncarrec("Client2", "producte_3", 1000);

    }
}
