
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.*;

/**
 *
 * @author gmartinez
 */
public class CreaBDIInicialitzaDades {

	/**
	 * @param args the command line arguments
	 */
	public static void createDBDefault() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
		try {
			Client[] clients = { new Client("Client1", "Adreça1", "e−mail1@domini.com", "+34911112211"),
					new Client("Client2", "Adreça2", "e−mail2@domini.com", "+34922223322"),
					new Client("Client3", "Adreça3", "e−mail3@domini.com", "+34933112233"),
					new Client("Client4", "Adreça4", "e−mail4@domini.com", "+34944112244") };

			clients[0].addComanda(new Encarrec("patates", 27));
			clients[0].addComanda(new Encarrec("ratoli", 100));
			clients[2].addComanda(new Encarrec("Impressora", 1));
			clients[2].addComanda(new Encarrec("Toner Impressora", 4));
			clients[2].addComanda(new Encarrec("Paquest A4", 20));

			for (int i = 0; i < clients.length; i++) {
				db.store(clients[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
										// encarrecs associats).
			}
		} finally {
			db.close();
		}
	}

	public static void insertProducts() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
		try {
			Producte[] productes = { new Producte("producte_1", 1337), new Producte("producte_2", 54),
					new Producte("producte_3", 666), new Producte("Ervas", 420) };

			for (int i = 0; i < productes.length; i++) {
				db.store(productes[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
										// encarrecs associats).
			}
		} finally {
			db.close();
		}
	}

	public static void main(String[] args) {
		createDBDefault();
		insertProducts();
	}

}
