/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;
/**
 *
 * @author gmartinez
 */
public class Encarrec {
    private Producte producteId;
    private int quantitat;
    private Date data;

    public Encarrec (Producte prod, int q) {
        producteId = prod;
        quantitat = q;
        data = new Date();
    }
    public Encarrec(String n, int q) {
        producteId = new Producte(n);
        quantitat = q;
        data = new Date();
    }



    public String getNom() {
        return producteId.getProducteNom();
    }
    public int getQuantitat() {
        return quantitat;
    }
    public Date getData() {
        return data;
    }
    public Producte getProducteId() {
        return producteId;
    }


    @Override
    public String toString() {
        return getData()+ " − " + getNom() + " (" + getQuantitat() + ")";
    }
}
