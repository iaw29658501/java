/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;


/**
 *
 * @author gmartinez
 */
public class VeureTots {
    private static void veureClients1() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        
        try { 
        	// SQL "WHERE"
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            ObjectSet<Client> result = db.query(p); 
            
            while (result.hasNext()) { 
                Client cli = result.next(); 
                System.out.println("Client: " + cli.getNom() + " " + "comandas: " + cli.getNreComandes());
            } 
        } finally { 
            db.close(); 
            System.out.println("veureClients1(): FINAL");
            System.out.println("--------");
        } 
    }     
    
    private static void veureClients2() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");

        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            List<Client> result = db.query(p); 
            
            for (Client cli : result) { 
                System.out.println("Client: " + cli.getNom());
            } 
        } finally { 
            db.close(); 
            System.out.println("veureClients2(): FINAL");
            System.out.println("--------");
        } 
    } 
    
    private static void veureProductes() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        
        try { 
        	// SQL "WHERE"
            Predicate p = new Predicate<Producte>() { 
                @Override 
                public boolean match(Producte c) { 
                	return true;
                } 
            }; 
            
            ObjectSet<Producte> result = db.query(p); 
            
            while (result.hasNext()) { 
                Producte prod = result.next(); 
                System.out.println("Producte: " + prod.getProducteNom() + " , " + prod.getProductePreu() + " (" + prod.getProducteId() + ")" ); 
            } 
        } finally { 
            db.close(); 
            System.out.println("veureProductes(): FINAL");
            System.out.println("--------");
        } 
    }
/*
    private static void veureByExemple() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        Client ex = new Client(null, "Adreça3", null, null);
        ObjectSet<Client> result = db.queryByExample(ex);
        while (result.hasNext()) {
            Client cli = result.next();
            System.out.println(cli.getNom());
        }
        db.close();
    }
*/
    public static void main(String[] args) throws Exception { 
    	veureClients1();
      //  veureClients2();
        //veureByExemple();
      // veureProductes();
    }

}
