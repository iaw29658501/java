import java.util.Objects;

public class Producte {
	
	public static int currentID = 1;
	
	public int producteId;
	public String producteNom;
	public int productePreu;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Producte producte = (Producte) o;
		return producteId == producte.producteId &&
				productePreu == producte.productePreu &&
				Objects.equals(producteNom, producte.producteNom);
	}

	@Override
	public int hashCode() {
		return Objects.hash(producteId, producteNom, productePreu);
	}

	// CONSTRUCTORES
	public Producte(int producteId, String producteNom, int productePreu) {
		super();
		this.producteId = producteId;
		this.producteNom = producteNom;
		this.productePreu = productePreu;
	}
	public Producte(String producteNom, int productePreu) {
		this(currentID, producteNom, productePreu);
		Producte.currentID++;
	}
	public Producte(String producteNom) {
		this(currentID, producteNom, 1337);
	}
	// GETTERS AND SETTERS

	public static int getCurrentID() {
		return currentID;
	}
	public static void setCurrentID(int currentID) {
		Producte.currentID = currentID;
	}
	public int getProducteId() {
		return producteId;
	}
	public void setProducteId(int producteId) {
		this.producteId = producteId;
	}
	public String getProducteNom() {
		return producteNom;
	}
	public void setProducteNom(String producteNom) {
		this.producteNom = producteNom;
	}
	public int getProductePreu() {
		return productePreu;
	}
	public void setProductePreu(int productePreu) {
		this.productePreu = productePreu;
	}

	@Override
	public String toString() {
		return "Producte{" +
				"producteId=" + producteId +
				", producteNom='" + producteNom + '\'' +
				", productePreu=" + productePreu +
				'}';
	}
}