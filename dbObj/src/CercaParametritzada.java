Client.java                                                                                         0000644 0001750 0001750 00000003016 13606076224 012205  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.*;
/**
 *
 * @author gmartinez
 */
public class Client {
    private String nom;
    private String aPostal;
    private String aElectronica;
    private String telefon;
    private List<Encarrec> liComandes = new LinkedList<Encarrec>();
    
    
    public Client(String n, String ap, String ae, String t) {
        nom = n;
        aPostal = ap;
        aElectronica = ae;
        telefon = t;
    }
    
    public String getNom() {
        return nom;
    }
    
    public String getAPostal() {
        return aPostal;
    }
    
    public String getAElectronica() {
        return aElectronica;
    }
    
    public void setAElectronica(String ae) {
        aElectronica = ae;
    }
    
    public String getTelefon() {
        return telefon;
    }
    
    public int getNreComandes() {
        return liComandes.size();
    }
    
    public void addComanda(Encarrec e) {
        liComandes.add(e);
    }
    
    public List<Encarrec> getComandes() {
        return liComandes;
    }
    
    
    @Override
    public String toString() {
        String res = nom + " : " + aPostal + " : (" + aElectronica + ", " + telefon + ")\n";
        Iterator<Encarrec> it = liComandes.iterator();
        while (it.hasNext()) {
            Encarrec e = it.next();
            res += e.toString() + "\n";
        }
        return res;
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  CreaBDIInicialitzaDades.java                                                                        0000644 0001750 0001750 00000003607 13606350630 015304  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.*;

/**
 *
 * @author gmartinez
 */
public class CreaBDIInicialitzaDades {

	/**
	 * @param args the command line arguments
	 */
	public static void createDBDefault() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
		try {
			Client[] clients = { new Client("Client1", "Adreça1", "e−mail1@domini.com", "+34911112211"),
					new Client("Client2", "Adreça2", "e−mail2@domini.com", "+34922223322"),
					new Client("Client3", "Adreça3", "e−mail3@domini.com", "+34933112233"),
					new Client("Client4", "Adreça4", "e−mail4@domini.com", "+34944112244") };

			clients[0].addComanda(new Encarrec("patates", 27));
			clients[0].addComanda(new Encarrec("ratoli", 100));
			clients[2].addComanda(new Encarrec("Impressora", 1));
			clients[2].addComanda(new Encarrec("Toner Impressora", 4));
			clients[2].addComanda(new Encarrec("Paquest A4", 20));

			for (int i = 0; i < clients.length; i++) {
				db.store(clients[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
										// encarrecs associats).
			}
		} finally {
			db.close();
		}
	}

	public static void insertProducts() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
		try {
			Producte[] productes = { new Producte("producte_1", 1337), new Producte("producte_2", 54),
					new Producte("producte_3", 666), new Producte("Ervas", 420) };

			for (int i = 0; i < productes.length; i++) {
				db.store(productes[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
										// encarrecs associats).
			}
		} finally {
			db.close();
		}
	}

	public static void main(String[] args) {
		createDBDefault();
		insertProducts();
	}

}
                                                                                                                         Encarrec.java                                                                                       0000644 0001750 0001750 00000001766 13606351332 012517  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;
/**
 *
 * @author gmartinez
 */
public class Encarrec {
    private Producte producteId;
    private int quantitat;
    private Date data;

    public Encarrec (Producte prod, int q) {
        producteId = prod;
        quantitat = q;
        data = new Date();
    }
    public Encarrec(String n, int q) {
        producteId = new Producte(n);
        quantitat = q;
        data = new Date();
    }



    public String getNom() {
        return producteId.getProducteNom();
    }
    public int getQuantitat() {
        return quantitat;
    }
    public Date getData() {
        return data;
    }
    public Producte getProducteId() {
        return producteId;
    }


    @Override
    public String toString() {
        return getData()+ " − " + getNom() + " (" + getQuantitat() + ")";
    }
}
          EsborraBDSencera.java                                                                               0000644 0001750 0001750 00000004370 13606105342 014071  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import java.util.List;
/**
 *
 * @author gmartinez
 */
public class EsborraBDSencera {
    private static void visualitzaDadesClients() {
        String valor;
        
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o"); 
        System.out.println("visualitzaDadesClients(): INICI"); 
        
        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            ObjectSet<Client> result = db.query(p); 
            
            while (result.hasNext()) { 
                Client cli = result.next(); 
                System.out.println("Client: " + cli); 
            } 
        } finally { 
            db.close(); 
            System.out.println("visualitzaDadesClients(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    private static void esborraTotaLaBD() {
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Client.class).cascadeOnDelete(true);
        ObjectContainer db = Db4oEmbedded.openFile(config, "baseDeDades/BDOOClients.db4o");        
        System.out.println("esborraTotaLaBD(): INICI");

        // Esborra tota la BD
        List result = db.queryByExample(new Object());
        /*
        while(result.hasNext()) {
            db.delete(result.next());
        }
        */
        for (Object cli : result) { 
            System.out.println("Esborrant: " + cli); 
            db.delete(cli);
        } 

        db.close();      
        System.out.println("esborraTotaLaBD(): FINAL");
        System.out.println("--------");
    }
    
    
    
    
    public static void main(String[] args) throws Exception { 
        visualitzaDadesClients();
        esborraTotaLaBD(); 
        visualitzaDadesClients();

    } 
}
                                                                                                                                                                                                                                                                        EsborraClient.java                                                                                  0000644 0001750 0001750 00000014472 13606352270 013531  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.*;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author gines
 */
public class EsborraClient {
    private static void visualitzaDadesClients() {
        String valor;
        
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o"); 
        System.out.println("visualitzaDadesClients(): INICI"); 
        
        try { 
        	// SELECT CLIENT WHERE NOM IS NOT NULL
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return c.getNom().length() > 0; 
                } 
            }; 
            
            ObjectSet<Client> result = db.query(p); 
            
            while (result.hasNext()) { 
                Client cli = result.next(); 
                System.out.println("Client: " + cli); 
            } 
        } finally { 
            db.close(); 
            System.out.println("visualitzaDadesClients(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    private static void esborraClientAmbCercaObjectesPerExemple(){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");

        Scanner in = new Scanner(System.in);
        System.out.print("Quin és nom del client? ");
        String nom = in.nextLine();

        //Cercar clients a la BDOO i obtenir−los a memòria com objectes del programa
        Client qbe = new Client(nom, null, null, null);
        ObjectSet<Client> clients = db.queryByExample(qbe);

        if (clients.size() != 1) {
            System.out.println("No es pot modificar aquest nom.");
        } else {
            Client c = clients.next();
            List<Encarrec> li = c.getComandes();
            Iterator<Encarrec> it = li.iterator();
            //Anem esborrant tots els encàrrecs, un per un
            while (it.hasNext()) {
                Encarrec e = it.next();
                db.delete(e);
            }
            //Ara, ja es pot esborrar el client
            db.delete(c);
        }
        db.close();
        System.out.println("esborraClientAmbCercaObjectesPerExemple(): FINAL");
        System.out.println("--------");
    }
    
    
    private static void esborraClientAmbCercaObjectesPerCercaNativa(){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");

        Scanner in = new Scanner(System.in);
        System.out.print("Quin és nom del client? ");
        final String nom = in.nextLine();

        //Cercar clients a la BDOO i obtenir−los a memòria com objectes del programa
        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return c.getNom().equalsIgnoreCase(nom); 
                } 
            }; 
            
            ObjectSet<Client> clients = db.query(p); 
            
            if (clients.size() != 1) {
                System.out.println("No es pot modificar aquest nom. N'hi ha més d'1 o 0.");
            } else {
                Client c = clients.next();
                
                List<Encarrec> li = c.getComandes();
                Iterator<Encarrec> it = li.iterator();
                //Anem esborrant tots els encàrrecs, un per un
                while (it.hasNext()) {
                    Encarrec e = it.next();
                    db.delete(e);
                }
                
                //Ara, ja es pot esborrar el client
                db.delete(c);
            }
        } finally { 
            db.close(); 
            System.out.println("esborraClientAmbCercaObjectesPerCercaNativa(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    private static void visualitzaElsEncarrecs(ObjectContainer db) {
        Predicate p2 = new Predicate<Encarrec>() { 
            @Override 
            public boolean match(Encarrec c) { 
                return true; 
            } 
        }; 

        ObjectSet<Encarrec> encarrecs = db.query(p2); 

        while (encarrecs.hasNext()) { 
            Encarrec encarrec = encarrecs.next(); 
            System.out.println("Encarrec: " + encarrec); 
        } 
    }
    
    private static void esborraClientAmbCercaObjectesPerCercaNativaAmbCascade(String nom){
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Client.class).cascadeOnDelete(true);
        ObjectContainer db = Db4oEmbedded.openFile(config, "baseDeDades/BDOOClients.db4o");

        System.out.println("visualitzaElsEncarrecs 1");
        visualitzaElsEncarrecs(db);
        
        //Cercar clients a la BDOO i obtenir−los a memòria com objectes del programa
        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return c.getNom().equalsIgnoreCase(nom); 
                } 
            }; 
            
            ObjectSet<Client> clients = db.query(p); 
            
            if (clients.size() != 1) {
                System.out.println("No es pot modificar aquest nom. N'hi ha més d'1 o 0.");
            } else {
                Client c = clients.next();
                
                db.delete(c);
                //Busquem tots els encàrrecs a veure si l'esborrar en cascada ha esborrar els 
                //encàrrecs d'un client a l'esborrar el client.
                System.out.println("visualitzaElsEncarrecs 2");
                visualitzaElsEncarrecs(db);
            }
        } finally { 
            db.close(); 
            System.out.println("esborraClientAmbCercaObjectesPerCercaNativaAmbCascade(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    public static void main(String[] args) throws Exception {
        visualitzaDadesClients();
        //esborraClientAmbCercaObjectesPerExemple();
        //esborraClientAmbCercaObjectesPerCercaNativa();
        esborraClientAmbCercaObjectesPerCercaNativaAmbCascade("Client1");
        visualitzaDadesClients();
    }
    
}
                                                                                                                                                                                                      Exercici.java                                                                                       0000644 0001750 0001750 00000013067 13606351371 012530  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;

import java.util.Scanner;


public class Exercici {
    // EXERCICIOS 3 y 4
    public static void ex3y4() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {

            // EXERCICIO 3
            Producte[] productes = {new Producte("producte_1", 1337), new Producte("producte_2", 54),
                    new Producte("producte_3", 666)};

            for (int i = 0; i < productes.length; i++) {
                db.store(productes[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
                // encarrecs associats).
            }

            Client[] clients = {new Client("client_1", "Adreça1", "e−mail1@domini.com", "+34911112211"),
                    new Client("client_1", "Adreça2", "e−mail2@domini.com", "+34922223322")};

            clients[0].addComanda(new Encarrec("patates", 27));
            clients[0].addComanda(new Encarrec("ratoli", 100));
            clients[1].addComanda(new Encarrec("Impressora", 1));
            clients[1].addComanda(new Encarrec("Toner Impressora", 4));

            for (int i = 0; i < clients.length; i++) {
                db.store(clients[i]); // S'emmagatzema clients[i] i automàticament totes les seves referències (els
                // encarrecs associats).
            }

        } finally {
            db.close();
        }
    }

    // EXERCICIO 5
    public static void listProducts() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            // SQL "WHERE"
            Predicate p = new Predicate<Producte>() {
                @Override
                public boolean match(Producte c) {
                    return true;
                }
            };
            ObjectSet<Producte> result = db.query(p);

            while (result.hasNext()) {
                Producte prod = result.next();
                System.out.println("Producte: " + prod.toString());
            }
        } finally {
            db.close();
            System.out.println("veureClients1(): FINAL");
            System.out.println("--------");
        }
    }

    /*
    public static Producte getProd(String prodnom){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            Predicate p = new Predicate<Producte>() {
                @Override
                public boolean match(Producte c) {
                    boolean b = c.getProducteNom().equalsIgnoreCase(prodnom);
                    return b;
                }
            };

            ObjectSet<Producte> result = db.query(p);
            if (result.hasNext()) {
                Producte e = result.next();
                return e;
            }
        } finally {
            db.close();
        }
        return null;
    }
    public static Client getClient(String clienom){
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        try {
            Predicate p = new Predicate<Client>() {
                @Override
                public boolean match(Client c) {
                    boolean b = c.getNom().equalsIgnoreCase(clienom);
                    return b;
                }
            };

            ObjectSet<Client> result = db.query(p);
            if (result.hasNext()) {
                Client e = result.next();
                return e;
            }
        } finally {
            db.close();
        }
        return null;
    }
    */
    // EXERCICIO 6
    // input: nombre del cliente , nombre del producto y cuantidad
    private static void afegirEncarrec(String nom, String prod, int quant) {
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Client.class).cascadeOnUpdate(true);
        ObjectContainer db = Db4oEmbedded.openFile(config, "baseDeDades/BDOOClients.db4o");

        //Cercar clients a la BDOO i obtenir−los a memòria com a objectes del programa
        try {
            Predicate p = new Predicate<Client>() {
                @Override
                public boolean match(Client c) {
                    return c.getNom().equalsIgnoreCase(nom);
                }
            };
            Predicate predprod = new Predicate<Producte>() {
                @Override
                public boolean match(Producte pr) {
                    return pr.getProducteNom().equalsIgnoreCase(prod);
                }
            };

            ObjectSet<Client> clients = db.query(p);
            ObjectSet<Producte> productes = db.query(predprod);
            if (clients.size() != 1 || productes.size() != 1) {
                System.out.println("No es pot modificar, no es unico! Clients.size:" + clients.size() + " prod.size" + productes.size() );
            } else {
                Encarrec ne = new Encarrec(productes.next(), quant);
                Client c = clients.next();
                c.addComanda(ne);
                db.store(c);
            }
        } finally {
            db.close();
            System.out.println("AfegirEncarrec(): FINAL");
            System.out.println("--------");
        }
    }


    public static void main(String[] args) {
         // ex3y4();
         // ex5 :
         // listProducts();
         afegirEncarrec("Client1", "producte_1", 10);
         afegirEncarrec("Client2", "producte_2", 1000);
         afegirEncarrec("Client1", "producte_3", 1000);
         afegirEncarrec("Client2", "producte_3", 1000);

    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ModificarDades.java                                                                                 0000644 0001750 0001750 00000010047 13606076224 013627  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.*;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import java.util.Scanner;

/**
 *
 * @author gines
 */
public class ModificarDades {
    private static void ModificaAElectronica() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        Scanner in = new Scanner(System.in);
        System.out.print("ModificaAElectronica(): Quin és nom del client? ");
        String nom = in.nextLine();

        //Cercar clients a la BDOO i obtenir−los a memòria com a objectes del programa
        //S’usa una cerca per exemple
        Client qbe = new Client(nom, null, null, null);
        ObjectSet<Client> clients = db.queryByExample(qbe);

        if (clients.size() != 1) {
            System.out.println("No es pot modificar aquest nom. N'hi ha més d'1 o 0.");
        } else {
            System.out.print("Quina és la nova adreça? ");
            String ad = in.nextLine();
            
            Client c = clients.next();
            c.setAElectronica(ad);
            db.store(c);
        }
        db.close();
        System.out.println("ModificaAElectronica(): FINAL");
        System.out.println("--------");
    }
    
    
    private static void AfegirEncarrec() {
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Client.class).cascadeOnUpdate(true);
        ObjectContainer db = Db4oEmbedded.openFile(config, "baseDeDades/BDOOClients.db4o");

        Scanner in = new Scanner(System.in);
        System.out.print("AfegirEncarrec(): Quin és nom del client? ");
        final String nom = in.nextLine();
        
        //Cercar clients a la BDOO i obtenir−los a memòria com a objectes del programa
        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return c.getNom().equalsIgnoreCase(nom); 
                } 
            }; 
            
            ObjectSet<Client> clients = db.query(p); 
            
            if (clients.size() != 1) {
                System.out.println("No es pot modificar aquest nom. N'hi ha més d'1 o 0.");
            } else {
                System.out.print("Quin és nom del producte? ");
                String prod = in.nextLine();
                System.out.print("Quants en vols encarregar? ");
                String txtQuan = in.nextLine();
                int quant = Integer.parseInt(txtQuan);

                Encarrec ne = new Encarrec(prod, quant);
                Client c = clients.next();
                c.addComanda(ne);
                db.store(c);
            }
        } finally { 
            db.close(); 
            System.out.println("AfegirEncarrec(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    private static void visualitzaDadesClients() {
        String valor;
        
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o"); 
        System.out.println("visualitzaDadesClients(): INICI"); 
        
        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            ObjectSet<Client> result = db.query(p); 
            
            while (result.hasNext()) { 
                Client cli = result.next(); 
                System.out.println("Client: " + cli); 
            } 
        } finally { 
            db.close(); 
            System.out.println("visualitzaDadesClients(): FINAL");
            System.out.println("--------");
        } 
    }
    
    
    
    
    public static void main(String[] args) throws Exception {
        visualitzaDadesClients();
        ModificaAElectronica();
        AfegirEncarrec();
        visualitzaDadesClients();
    }

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         Producte.java                                                                                       0000644 0001750 0001750 00000003410 13606351423 012547  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  import java.util.Objects;

public class Producte {
	
	public static int currentID = 1;
	
	public int producteId;
	public String producteNom;
	public int productePreu;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Producte producte = (Producte) o;
		return producteId == producte.producteId &&
				productePreu == producte.productePreu &&
				Objects.equals(producteNom, producte.producteNom);
	}

	@Override
	public int hashCode() {
		return Objects.hash(producteId, producteNom, productePreu);
	}

	// CONSTRUCTORES
	public Producte(int producteId, String producteNom, int productePreu) {
		super();
		this.producteId = producteId;
		this.producteNom = producteNom;
		this.productePreu = productePreu;
	}
	public Producte(String producteNom, int productePreu) {
		this(currentID, producteNom, productePreu);
		Producte.currentID++;
	}
	public Producte(String producteNom) {
		this(currentID, producteNom, 1337);
	}
	// GETTERS AND SETTERS

	public static int getCurrentID() {
		return currentID;
	}
	public static void setCurrentID(int currentID) {
		Producte.currentID = currentID;
	}
	public int getProducteId() {
		return producteId;
	}
	public void setProducteId(int producteId) {
		this.producteId = producteId;
	}
	public String getProducteNom() {
		return producteNom;
	}
	public void setProducteNom(String producteNom) {
		this.producteNom = producteNom;
	}
	public int getProductePreu() {
		return productePreu;
	}
	public void setProductePreu(int productePreu) {
		this.productePreu = productePreu;
	}

	@Override
	public String toString() {
		return "Producte{" +
				"producteId=" + producteId +
				", producteNom='" + producteNom + '\'' +
				", productePreu=" + productePreu +
				'}';
	}
}                                                                                                                                                                                                                                                        VeureTotsElsClients.java                                                                            0000644 0001750 0001750 00000006505 13606352542 014723  0                                                                                                    ustar   lover                           lover                                                                                                                                                                                                                  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.List;


/**
 *
 * @author gmartinez
 */
public class VeureTotsElsClients {
    private static void veureClients1() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        
        try { 
        	// SQL "WHERE"
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            ObjectSet<Client> result = db.query(p); 
            
            while (result.hasNext()) { 
                Client cli = result.next(); 
                System.out.println("Client: " + cli.getNom() + " " + "comandas: " + cli.getNreComandes());
            } 
        } finally { 
            db.close(); 
            System.out.println("veureClients1(): FINAL");
            System.out.println("--------");
        } 
    }     
    
    private static void veureClients2() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");

        try { 
            Predicate p = new Predicate<Client>() { 
                @Override 
                public boolean match(Client c) { 
                    return true; 
                } 
            }; 
            
            List<Client> result = db.query(p); 
            
            for (Client cli : result) { 
                System.out.println("Client: " + cli.getNom());
            } 
        } finally { 
            db.close(); 
            System.out.println("veureClients2(): FINAL");
            System.out.println("--------");
        } 
    } 
    
    private static void veureProductes() { 
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        
        try { 
        	// SQL "WHERE"
            Predicate p = new Predicate<Producte>() { 
                @Override 
                public boolean match(Producte c) { 
                	return true;
                } 
            }; 
            
            ObjectSet<Producte> result = db.query(p); 
            
            while (result.hasNext()) { 
                Producte prod = result.next(); 
                System.out.println("Producte: " + prod.getProducteNom() + " , " + prod.getProductePreu() + " (" + prod.getProducteId() + ")" ); 
            } 
        } finally { 
            db.close(); 
            System.out.println("veureProductes(): FINAL");
            System.out.println("--------");
        } 
    }
/*
    private static void veureByExemple() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BDOOClients.db4o");
        Client ex = new Client(null, "Adreça3", null, null);
        ObjectSet<Client> result = db.queryByExample(ex);
        while (result.hasNext()) {
            Client cli = result.next();
            System.out.println(cli.getNom());
        }
        db.close();
    }
*/
    public static void main(String[] args) throws Exception { 
    	veureClients1();
      //  veureClients2();
        //veureByExemple();
      // veureProductes();
    }

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           