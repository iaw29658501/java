package examen_DAW;

import java.time.LocalDateTime;
import java.util.Comparator;

public class Naus_Dades implements Comparator<Naus_Dades> {
	String nom;
	String tipus;
	String model;
	LocalDateTime dataConstruccio;
	String descripcio;

	public Naus_Dades(String nom, String tipus, String model, LocalDateTime dataConstruccio, String descripcio) {
		super();
		this.nom = nom;
		this.tipus = tipus;
		this.model = model;
		this.dataConstruccio = dataConstruccio;
		this.descripcio = descripcio;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTipus() {
		return tipus;
	}

	public void setTipus(String tipus) {
		this.tipus = tipus;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public LocalDateTime getDataConstruccio() {
		return dataConstruccio;
	}

	public void setDataConstruccio(LocalDateTime dataConstruccio) {
		this.dataConstruccio = dataConstruccio;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	@Override
	public String toString() {
		return nom + " { tipus=" + tipus + ", model=" + model + ", dataConstruccio=" + dataConstruccio + ", descripcio="
				+ descripcio + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipus == null) ? 0 : tipus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Naus_Dades other = (Naus_Dades) obj;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (tipus == null) {
			if (other.tipus != null)
				return false;
		} else if (!tipus.equals(other.tipus))
			return false;
		return true;
	}

	@Override
	public int compare(Naus_Dades arg0, Naus_Dades arg1) {
		return arg0.getNom().compareTo(arg1.getNom());
	}

}
