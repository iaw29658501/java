package examen_DAW;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.Iterator;

public class Naus {

	public static Set<Naus_Dades> hashSetDeNaus;

	public static void menu_23() {
		System.out.println("Text que voleu buscar dins de la descripció: ");
		Scanner in = new Scanner(System.in);
		String searchText = in.nextLine();

		for (Naus_Dades nau : hashSetDeNaus) {
			if (nau.getDescripcio().contains(searchText)) {
				System.out.println(nau.getNom());
				System.out.println("\tTipus = " + nau.getTipus());
				System.out.println("\tModel = " + nau.getModel());
				System.out.println("\tData construcció = " + nau.getDataConstruccio());
				System.out.println("\tDescripcio = " + nau.getDescripcio());
			}
		}

	}

	public static void menu_24() {
		LinkedList<Naus_Dades> linkedListDeNaus = new LinkedList<Naus_Dades>(hashSetDeNaus);
		NausComparadorTipus comp = new NausComparadorTipus();
		Collections.sort(linkedListDeNaus, comp);

		for (Naus_Dades nau : linkedListDeNaus) {
			System.out.println(nau.getNom());
			System.out.println("\tTipus = " + nau.getTipus());
			System.out.println("\tModel = " + nau.getModel());
			System.out.println("\tData construcció = " + nau.getDataConstruccio());
			System.out.println("\tDescripcio = " + nau.getDescripcio());
		}

	}

	public static void menu_31() {
		System.out.println("------------------- MENU 31 (eliminarNausVsDataConstruccio)-------------");
		System.out.println("Data fins a la qual s'esborraran les naus [dd-mm-yyy]: ");
		Scanner in = new Scanner(System.in);
		String dataInput = in.nextLine();

		DateTimeFormatter formater = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate temp = LocalDate.parse(dataInput, formater);
		LocalDateTime dataTmp = temp.atStartOfDay();

		for (Iterator<Naus_Dades> iterator = hashSetDeNaus.iterator(); iterator.hasNext();) {
			Naus_Dades nau = iterator.next();
			System.out.print(nau.getNom() + " (" + nau.tipus + " del model " + nau.getModel());
			System.out.print(" construit en " + nau.getDataConstruccio().getDayOfMonth() + "-"
					+ nau.getDataConstruccio().getMonthValue() + "-" + nau.getDataConstruccio().getYear());
			if (nau.getDataConstruccio().isBefore(dataTmp)) {
				System.out.println("   ¡¡¡ESBORRADA!!!");
				iterator.remove();
			}
		}

	}

	public static void menu_32() {
		System.out.println("------------------- MENU 32 (eliminarNausVsDataConstruccioRemoveIf)-------------");
		System.out.println("Data fins a la qual s'esborraran les naus [dd-mm-yyy]: ");
		Scanner in = new Scanner(System.in);
		String dataInput = in.nextLine();

		DateTimeFormatter formater = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate temp = LocalDate.parse(dataInput, formater);
		LocalDateTime dataTmp = temp.atStartOfDay();

		Predicate<Naus_Dades> nausDadesPredicate = p -> p.getDataConstruccio().isBefore(dataTmp);
		hashSetDeNaus.removeIf(nausDadesPredicate);

		menu_2();
	}

	////////////// EXAMEN 1 /////////////

	// Me falata UN metodo en UNA linea xD
	public static void menu_21(HashMap hashMapNumNausVsTipus) {
		// no me dio tiempo de encontrar el metodo entrySet
		Set<Map.Entry<String, Integer>> naus = hashMapNumNausVsTipus.entrySet();
		Iterator<Entry<String, Integer>> it = naus.iterator();
		while (it.hasNext()) {
			Entry<String, Integer> x = it.next();
			System.out.println("Tipe:" + x.getKey() + "\t Cuantidad: " + x.getValue());
		}
	}

	public static void menu_22() {
		Scanner sc = new Scanner(System.in);
		int inputUser = -1;
		// validador de input
		boolean inputAceptable = false;

		ArrayList<Naus_Dades> arrayListDelSetDeNaus = new ArrayList<>(hashSetDeNaus);
		// El TreeSet que sera de Strings ordenada segundo el comparaTo de String
		TreeSet<String> llistaDeTipus = new TreeSet<>(String::compareToIgnoreCase);
		ListIterator<Naus_Dades> liter = arrayListDelSetDeNaus.listIterator();

		// creando la lista de tipus
		while (liter.hasNext()) {
			llistaDeTipus.add(liter.next().getTipus());
		}

		System.out.println("--------MENU 22--------");
		System.out.print("Escoja un tipu para listar:\n");

		int indexOpciones = 0;
		// Listar opciones
		for (String tipus : llistaDeTipus) {
			System.out.printf("%d. %s\n", indexOpciones, tipus);
			indexOpciones++;
		}
		System.out.println("\n50. Salir MENU para listar");
		System.out.print("\nOpciones? ");
		// Get Input sin petar
		while (!inputAceptable) {
			try {
				inputUser = sc.nextInt();
			} catch (Exception ex) {
			}
			// opcion de salida
			if (inputUser == 50) {
				return;
			}
			// Verificacion del input
			inputAceptable = (inputUser >= 0 && inputUser < indexOpciones);
			// Si el input no es aceptable avise sobre cuales son
			if (!inputAceptable) {
				System.out.printf("Este tipo no existe, selecciona de 0 a %s: ", indexOpciones - 1);
			}
		}
		// Listando todas las naves de la array que sean del tipo pedido
		String tipus = (new ArrayList<String>(llistaDeTipus)).get(inputUser);
		indexOpciones = 1;
		liter = arrayListDelSetDeNaus.listIterator();
		while (liter.hasNext()) {
			Naus_Dades nau = liter.next();
			if (nau.getTipus().equalsIgnoreCase(tipus)) {
				System.out.printf("%d: %s\n", indexOpciones++, nau);
			}

		}

	}

}
