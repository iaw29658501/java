package examen_DAW;

import java.util.Comparator;

public class NausComparadorTipus implements Comparator<Naus_Dades> {

	public int compare(Naus_Dades arg0, Naus_Dades arg1) {
		int result = arg0.getTipus().compareTo(arg1.getTipus());
		if (result == 0) {
			result = arg0.getModel().compareTo(arg1.getModel());
		}
		return result;
	}
	
}
