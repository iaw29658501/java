package practica;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Main {

public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> numbs = new ArrayList<Integer>();

		for (String arg : args) {
			numbs.add(Integer.parseInt(arg));
		}
		System.out.println("Size : " + numbs.size());

		for (int i = 0; i < numbs.size(); i++) {
			numbs.set(i, (numbs.get(i) * 2));
		}

		for (int i = 0; i < numbs.size(); i++) {
			System.out.println(numbs.get(i));
		}

		Iterator<Integer> it = numbs.iterator();
		while (it.hasNext()) {
			if (it.next() > 100) {
				it.remove();
			}
		}
		Collections.sort(numbs);

		for (Integer numb : numbs) {
			System.out.println(numb);
		}

		// Exercicio 2
		List<Integer> numbs2 = new ArrayList<Integer>();
		numbs2.add(10);
		numbs2.add(20);
		numbs2.add(30);
		numbs.addAll(numbs2);
		System.out.println();

		for (Integer numb : numbs2) {
			System.out.println(numbs.contains(numb));
		}
		numbs2.clear();
		for (Integer numb : numbs2) {
			System.out.println(numb);
		}

		Persona uno = new Persona(19,"Eddy", "1");

		Persona dos= new Persona(17,"Pol", "2");

		Persona tres = new Persona(21,"Marc", "3");
		
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(uno);
		personas.add(dos);
		personas.add(tres);
		System.out.println();
		PersonaList.printList(personas);
		
		System.out.println();
		
		System.out.println("Menores de la lista no ordenada:");
		PersonaList.printList(PersonaList.menorsEdat(personas));
		
		uno = new Persona(11,"Eddy", "1");

		dos= new Persona(17,"Pol", "2");

		tres = new Persona(18,"Marc", "3");
		
		Persona cuatro = new Persona(21,"John", "4");
		
		personas = new ArrayList<Persona>();
		
		personas.add(uno);
		personas.add(dos);
		personas.add(tres);
		personas.add(cuatro);
		
		System.out.println("\n\nMenores de lista ordenada");
		PersonaList.printList(PersonaList.menorsEdat2(personas));
		
	}

}

