package practica;


public class Persona implements Comparable<Persona>{
	Integer age;
	String name;
	String dni;
	
	
	
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public Persona(Integer age, String name, String dni) {
		super();
		this.age = age;
		this.name = name;
		this.dni = dni;
	}
	@Override
	public int compareTo(final Persona a) {
		return this.getDni().compareTo(a.getDni());
	}
	
}