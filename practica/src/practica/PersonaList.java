package practica;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PersonaList {

	public static final int MAJOR_EDAT = 18;
	
	public static void printList(List<Persona> personas) {
		for ( Persona p : personas) {
			System.out.println(p.getName());
		}
	}
	
	public static List<Persona> menorsEdat(List<Persona> personas) {
		List<Persona> menors = new ArrayList<Persona>();
		for (Persona p : personas) {
			if (p.getAge() < MAJOR_EDAT) {
				menors.add(p);
			}
		}
		return menors;
	}
	
	public static List<Persona> menorsEdat2(List<Persona> personas) {
		List<Persona> menors = new ArrayList<Persona>();
		Iterator<Persona> it = personas.iterator();
		int position = 0;
		boolean esMenor = true;
		
		while (it.hasNext() && esMenor) {
			position++;
			if (it.next().getAge() >= 18) {
				esMenor = false;
			}
		}
		return personas.subList(0, position-1 );
	}
	
	
	
}
