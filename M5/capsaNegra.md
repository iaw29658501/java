# Capsa Negra


### Analisis limit value
| Condicion de entrada        | Classes equivalencia validas| no validas| Valores limit |
| -------------------         |:---------------------------:| ---------:|---------------|
| 4 o 5 digitos               | nº de digitos = 4 o 5 (1)     | n digitos < 4 (2) o n dig > 5 (3) | nº dígits = 3 (2) o nº dígits = 4 (1) o nº dígits = 5 (1) o nº dígits = 6 (3)    |
| 2 primeros digitos entre 1 y 52| dig[0:2] <= 52 && dig[0:2] >=1 (4)| (dig[0:2] > 52 \|\| dig[0:2] < 1) (5) |  (4) x=01 ; x=02; x=51; x = 52    (5) x= 00; x= 53               |
| 3 ultimos digitos de indican un municipio que deve existir en esta provincia | x[-3:] = 000      |    x[-3] = inexistente |            |



| Identificador  | Classes equivalència  cobertes | Sortida esperada |
| ---------------|----------------------| ------------------|
| 1234 | 2 | ERROR. Format incorrecte|
|123456|3|ERROR. Format incorrecte|
|17707|1, 4, 7|Girona - Agullana
|17000|6|Girona - {}
|17017|8|ERROR. Municipi∄
|17999|8|ERROR. Municipi∄
|55000 / 55123|5|ERROR. Província∄
|00000 / 00123|5a|ERROR. Província∄
|01000|4a, 6|Àlava- {}
|01123 (invent)|5, 5b|ERROR. Província∄
|52000|4c, 6|Melilla - {}
|52123 (invent)|4c, 7|Melilla - Invent
|53000 / 53123|5b,6 /7|ERROR. Província∄
