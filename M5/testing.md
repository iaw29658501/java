## 1
### Pruebas funcionales
Una prueba funcional es una prueba de tipo caja negra basada en la ejecución, revisión y retroalimentación de las funcionalidades previamente diseñadas para el software.
Son pruebas especificas, concretas y exhaustivas para verificar de que el software funciona como deveria
Tienen algunas fases, la análisis de requisitos (Planificación), diseño del plan de pruebas (Preparación), Ejecución y Gestión de Incidencias(Defectos)
Puedes ser manuales o automaticas, y de varios tipos como : pruebas exploratorias, pruebas de regesión, de compatibilidad, de itegridad o de aceptacion.
Con metodologia estándar (o en cascada) o segundo las metodologías ágiles

## 2
### Unitarias
Consisteixen a realitzar proves sobre els components o unitats més petits del codi font d'una aplicació i idenpendents dels altres.
(entès com a unitat la part provable més petita d'una aplicació)

### Integracio
testejan les unitats combinades, su combinació
(en anglès _Integration Testing_) anomenades també proves d'integració de components, són un tipus de proves de programari que tenen com a objectiu verificar el funcionament conjunt de les unitats provades individualment (classes, components, etc)
Es realitzen després de les proves unitàries i abans de les proves de sistema, i poden executar-se de forma incremental a mesura que es van finalitzant les diferents unitats
Principlament, en aquestes proves es detecten defectes a les interfícies i en la interacció entre els diferents components integrats.
Per a crear-les s'usen tècniques tant de caixa blanca com de caixa negra


### Sistema
Testeja tot el sistema, tot el cicle
Són els processos que permeten verificar i revelar la qualitat d'un producte programari. Són utilitzades per a identificar possibles errors, fallades d'implementació, qualitat, o usabilitat d'un programa d'ordinador o videojoc

### Acceptació
Testeja l'acceptació per part del client i obtenir l'seva

### Alpha
Test efectuats pel client en l'entorn de desenvolupament

### Beta
Test efectuats pel client en un entorn real

### Regressió
Tests que permeten veure que un canvi en el software no afecta inesperadament a d'altres parts( que no espatllem res)


## Proves no funcionales
De càrrega, d'stress, de performance, d'usabilitat, de seguritat, de portabilitat
### De càrrega (load testing)
per descobrir en quin punt deixe de funcionar el software carrejand al màxim l'sistema

### D'stress
aplicar condicions qanormals per analitzar l'comportament del programari

### D'usabilitat
comprabar si es "user friendly", si la interficie de usuario es logica y intuitiva

### De seguretat
Busqueda por bugs y vulnerabilidades

### De portabilitat
Comprobar que la facilitat o dificultat de instalar el programari en altres sistemes

## Diferencias
una se puede realizar manualmente y las
Una define que hace el producto y la otra como lo hace
