/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExercicisExcepcions;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gines
 */
public class Exercici4i5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Exercici1part1.exercici1Apartats1i2();
        //Exercici1part2.funcio1();
        //Exercici2.exercici2();
        //Exercici3.exercici3();
         
        System.out.println("----INICI----");
        Exercici4 persona = new Exercici4(6);
        //try {
            System.out.println("----------------");
            System.out.println("persona.setEdat(5):");
            persona.setEdat(5);
            System.out.println("----------------");
            System.out.println("persona.setEdat(-3):");
            persona.setEdat(-3);
            System.out.println("----------------");
            System.out.println("persona.setEdat(115):");
            persona.setEdat(115);
            System.out.println("----------------");
            System.out.println("persona.setEdat(27):");
            persona.setEdat(27);
            System.out.println("----FINAL----");
        //} catch (IllegalArgumentException("IllegalArgumentException")) {
        //        
        //}
        
        /*
        System.out.println("----INICI----");
        Exercici5 persona = new Exercici5(6);
        try {
            System.out.println("----------------");
            System.out.println("persona.setEdat(5):");
            persona.setEdat(5);
            System.out.println("----------------");
            System.out.println("persona.setEdat(-3):");
            persona.setEdat(-3);
            System.out.println("----------------");
            System.out.println("persona.setEdat(115):");
            persona.setEdat(115);
            System.out.println("----------------");
            System.out.println("persona.setEdat(27):");
            persona.setEdat(27);
            System.out.println("----FINAL----");
        } catch (Exercici5ValidarEdatException ex) {
            Logger.getLogger(Exercici4i5.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }
    
}
