package baraja;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import people.E;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> baraja =getbaraja();
		//print(baraja);
		Collections.shuffle(baraja);
		//System.out.println("La baraja barrejada és");
		//print(baraja);
		repartirJugadors(baraja, 2,5);
		System.out.println("La baraja restant és: ");
		print(baraja);
	}

	public static List<String> getbaraja() {
		String[] nipes = { "Espadas", "Corazones", "Rombos", "Treboles" };
		String[] valores = { "As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

		List<String> baraja = new ArrayList<String>();

		for (String i : valores) {
			for (String j : nipes) {
				baraja.add(i + " de " + j);
			}
		}
		return baraja;
	}

	public static List<String> repartir(List<String> baraja, int cartas) {
		int n = cartas;
		List<String> subList = baraja.subList(baraja.size() - n, baraja.size());
		List<String> ma = new ArrayList<String>(subList);
		baraja.subList(baraja.size() - n, baraja.size()).clear();

		return ma;
	}

	public static void repartirJugadors(List<String> baraja, int jugadores, int cartas) {

		if (jugadores * cartas > baraja.size()) {
			System.out.println("No se tienen cartas suficientes");
			return;
		}

		for (int i = 0; i < jugadores; i++) {
			print(repartir(baraja, cartas));
		}

	}


	public static void print(List<String> cartas){
		for (String carta:cartas){
			System.out.println(carta);
		}
	}
}
