package ejerciciosArrayStack;

import java.util.ArrayDeque;

public class ReverseStack {

	public static void main(String[] args) {

		ArrayDeque<Integer> st = new ArrayDeque<Integer>();
		st.push(1);
		st.push(2);
		st.push(3);
		System.out.println(st.toString());
		reverse(st);
	}	
	
	public static void reverse(ArrayDeque st) {
		ArrayDeque<Integer> st2 = new ArrayDeque<Integer>();
		while ( ! st.isEmpty() ) {
			st2.push( (int) st.pop() );
		}
		System.out.println(st2.toString());
	}

}
