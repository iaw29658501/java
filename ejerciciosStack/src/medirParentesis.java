import java.util.ArrayDeque;
import java.util.ArrayList;

public class medirParentesis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> l = new ArrayList<String>();
		String[] str = { "25+3*(1+2+(30*4))/2", "25+3*(1+2+(30*4))/(2", "25+3*(1+2+(30*4))/)2(",
				"25+3*(1+2)+(30*4))/2" };
		for (String string : str) {
			for (char ch : string.toCharArray()) {
				if (ch == '(') {
					l.add("(");
				} else if (ch == ')') {
					l.add(")");
				}
			}
			ArrayDeque<Integer> st = new ArrayDeque<Integer>();

			for (String s : l) {
				if (s.equals("(")) {
					st.push(1);
				} else if (s.equals(")")) {
					try {
						st.pop();
					} catch (Exception e) {
						System.out.println("La equacion " + string + " esta mal hecha");
						break;
					}
				}
			}
			if (st.size() != 0) {
				System.out.println("La equacion " + string + " esta mal hecha");
			} else {
				System.out.println("La equacion " + string + " esta bien hecha");
			}

		}
	}

}
