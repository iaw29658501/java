package firstSpring;

public class Electronic implements Tripulants {

	private InformeInterface informeNou;
	private String email;
	private String nomDepartament;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomDepartament() {
		return nomDepartament;
	}

	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "Aquestes són les terees del electronic";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "Electronic informe " + informeNou.getInforme();
	}

}
