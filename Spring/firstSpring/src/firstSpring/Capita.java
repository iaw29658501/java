package firstSpring;

public class Capita implements Tripulants {

	InformeInterface informenou;
	@Override
	public String agafarTarees() {
		return "Aquestes són les terees del capità";
	}
	@Override
	public String agafarInforme() {
		return "Informe del capità " + this.informenou.getInforme();
	}

	public Capita(InformeInterface informenou) {
		super();
		this.informenou = informenou;
	}

	public InformeInterface getInformenou() {
		return informenou;
	}

	
	
	
}
