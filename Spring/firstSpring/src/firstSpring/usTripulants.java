package firstSpring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usTripulants {
	public static void main(String[] args) {
		//Capita capita = new ...
	// 	File with the first tests
		System.out.println("BEFORE LOADING CONTEXT");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		System.out.println("AFTER CONTEXT LOADED");
		
		Tripulants capita = context.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());
		Tripulants maquinista = context.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());
		
		Tripulants electronic = context.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());
		
		System.out.println(capita.agafarInforme());
		System.out.println(maquinista.agafarInforme());
		System.out.println(electronic.agafarInforme());
		
		Electronic electronic2 = (Electronic) electronic;
		
		InformeInterface informe = context.getBean("informe", InformeInterface.class); 
		
		electronic2.setInformeNou(informe);
		System.out.println(electronic2.agafarInforme());
		System.out.println(electronic2.getEmail());
		
		Maquinista maquinista2 = (Maquinista) maquinista;
		
		System.out.println(maquinista2.getEmail());
		


		context.close();
			
	}
}
