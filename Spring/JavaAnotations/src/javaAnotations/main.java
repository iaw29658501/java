
package javaAnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("====== INICI MAIN ========");
		System.out.println("Testing if singletons creations are the same");
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		Tripulants doctor = contexte.getBean("doctor", Doctor.class);
		System.out.println(doctor.agafarInforme());
		Tripulants infermeria = contexte.getBean("infermeria", Infermera.class);
		System.out.println(infermeria.agafarInforme());
	}
}
