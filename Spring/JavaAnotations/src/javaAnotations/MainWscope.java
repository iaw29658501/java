package javaAnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainWscope {

	// MIRAR QUE DOS SINGLETON SON IGUALES
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("-----------------------------------");
		System.out.println();
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		Tripulants doctor1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor2 = contexte.getBean("doctorNau", Doctor.class);
		System.out.println("doctors are the same? " + (doctor1==doctor2));
		
		System.out.println("\n\n\n");
		
		
		
	}

}
