package javaAnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotations {

	public static void main(String[] args) {
		System.out.println("----------------usJavaAnnotations INICI");
		System.out.println();
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		Tripulants doctor = ctx.getBean("doctorNau", Doctor.class);
		System.out.println(doctor.agafarTarees());
		System.out.println(doctor.agafarInforme());
		System.out.println("-------------------");
		ctx.close();
		System.out.println("---------- usJavaAnnotations FIs");
	}

}
