package curs_de_05_AplicacionesWeb;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormulariWebControlador {

	@RequestMapping("/respostaDelFormulari")
	public String respostaDelFormulari() {
		return "formulariNomResposta";
	}

	@RequestMapping("/mostrarFormulari")
	public String mostrarFormulari() {
		return "formulariNom";
	}

	@RequestMapping("/respostaDelFormulari_29")
	public String processarFormulari(HttpServletRequest req, Model model) {
		String nom = req.getParameter("tripulantNom_29");
		String resp = "Qui hi ha (video 29) ? " + nom + " és un tripulant de la CCCP Leonov";
		model.addAttribute("missatgePerRetornar_29", resp);
		return "formulariNomResposta";
	}

	@RequestMapping("/respostaDelFormulari_32")
	public String processarFormulari(@RequestParam("tripulantNom_32") String nomTripulant, Model model) {
		String resp = "Qui hi ha (video 32) ? " + nomTripulant + " és un tripulant de la CCCP Leonov";
		model.addAttribute("missatgePerRetornar_32", resp);
		return "formulariNomResposta";
	}
}
