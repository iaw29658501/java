package practica01_04;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Organitzacio {
	String nom;
	InformeDepartament informe;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public InformeDepartament getInforme() {
		return informe;
	}
	public void setInforme(InformeDepartament informe) {
		this.informe = informe;
	}
}
