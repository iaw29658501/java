package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usPractica01_04 {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext_Practica01_04.xml");
		Organitzacio dpt = ctx.getBean("organitzacioB", Organitzacio.class);

		System.out.println(dpt.getNom());
	}
}
