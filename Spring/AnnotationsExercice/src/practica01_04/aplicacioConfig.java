package practica01_04;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("practica01_04")
@PropertySource("classpath:practica01_04/applicationContext_Practica01_04.propietats")
public class aplicacioConfig {
	
	@Bean
	public Organitzacio organitzacio() {
		return new Organitzacio();
	}
	
}
