package practica01_04;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class usExerciciMultiparametres {

	public static void main(String[] args) {
	
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		System.out.println("------ Exemple d'inserció obj's <> inicialitzats amb 1 paràmetre. ------");
		// https://stackoverflow.com/questions/55096855/how-to-ask-for-prototype-bean-in-spring-service-class-without-applicationcontext
		// Al final de todo, la respuesta del 11 de marzo del 2019 de Andy Brown.
		ServiceClass serviceClass = contexteAmbClasseConfig.getBean("serviceClass", ServiceClass.class);
		ArrayList<String> someArrayList = new ArrayList();
		someArrayList.add("patata");
		someArrayList.add("voladora");
		someArrayList.add("mortal");
		serviceClass.demoMethod(someArrayList);
		
		System.out.println("serviceClass.getPrototypeBeans().toString() = " + serviceClass.getPrototypeBeans().toString());
		
		List<ProtoTypeBean> protoTypeBeanLLista = new ArrayList<ProtoTypeBean>();
		protoTypeBeanLLista = (ArrayList) serviceClass.getPrototypeBeans();
		
		int i = 1;
		for(ProtoTypeBean protoTypeBeanTmp : protoTypeBeanLLista) {
			System.out.println(i + ": protoTypeBeanTmp = " + protoTypeBeanTmp + "; protoTypeBeanTmp.getParam() = " + protoTypeBeanTmp.getParam());
			i++;
		}
		
		contexteAmbClasseConfig.close();
	}

}
