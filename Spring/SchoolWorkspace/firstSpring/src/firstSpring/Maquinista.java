package firstSpring;

public class Maquinista implements Tripulants {

	private Informe informe;
	private String email;
	private String nomDepartament;
	
	
	

	public Maquinista(Informe informe) {
		super();
		this.informe = informe;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomDepartament() {
		return nomDepartament;
	}

	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "Aquestes són les terees del maquinista";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "informe de maquinista";
	}

}
