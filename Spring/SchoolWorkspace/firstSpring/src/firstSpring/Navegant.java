package firstSpring;

public class Navegant implements Tripulants {
	private InformeInterface informeNou;

	public InformeInterface getInformeNou() {
		return informeNou;
	}

	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}
	@Override
	public String agafarTarees() {
		return "Aquestes són les terees del capità";
	}
	@Override
	public String agafarInforme() {
		return "Informe del capità " + this.informeNou.getInforme();
	}
	public void metodeInit() {
		System.out.println("running init() from Navegant.class before bean is ready");
	}
	// the bean can only execute the destroy() if the bean is singleton
	public void metodeDestroy() {
		System.out.println("running destroy() from Navegant.java after bean beeing destroyed");
	}
	
}
