package firstSpring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class scopeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext_SingletonPrototype.xml");

		Tripulants capita = context.getBean("tripulantCapita", Tripulants.class);
		Tripulants maquinista = context.getBean("tripulantMaquinista", Tripulants.class);
		Tripulants electronic = context.getBean("tripulantElectronic", Tripulants.class);
		Tripulants capita2 = context.getBean("tripulantCapita", Tripulants.class);
		Tripulants maquinista2 = context.getBean("tripulantMaquinista", Tripulants.class);
		Tripulants electronic2 = context.getBean("tripulantElectronic", Tripulants.class);
		
		System.out.println(capita==capita2);
		System.out.println(maquinista==maquinista2);
		System.out.println(electronic==electronic2);
		
		System.out.println("TESTING INIT AND DESTROY (before getBean)");
		Navegant navegant = context.getBean("tripulantNavegant", Navegant.class);
		System.out.println("AFTER singleton bean with init");
		System.out.println(navegant.agafarInforme());
		System.out.println("after agafarInforme()");
		System.out.println(navegant.agafarTarees());
		System.out.println("after agafarTarees()");
		context.close();

	}
}
