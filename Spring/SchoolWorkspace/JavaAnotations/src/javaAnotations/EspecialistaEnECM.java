package javaAnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class EspecialistaEnECM implements Tripulants {
	private InformeElectronicaInterface informeEspecialistaEnESM;
	private InformeElectronicaInterface informeEspecialistaEnEPM;
	private InformeElectronicaInterface informeEspecialistaEnECM;
	@Value("${emailDepartamentECM}")
	private String email;
	@Value("${departamentECMNom}")
	private String departamentNom;
	
	public InformeElectronicaInterface getInformeEspecialistaEnECM() {
		return informeEspecialistaEnECM;
	}


	@Autowired
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnESM,
			InformeElectronicaInterface informeEspecialistaEnEPM,
			InformeElectronicaInterface informeEspecialistaEnECM) {
		super();
		this.informeEspecialistaEnESM = informeEspecialistaEnESM;
		this.informeEspecialistaEnEPM = informeEspecialistaEnEPM;
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}



	public String imprimirTripleInforme() {
		String cadena = "EspecialistaEnECM: imprimirTripleInforme():\n\t" +
				"Informe ECM: " + informeEspecialistaEnECM.getInformeElectronica() + "\n \t" +
				"Informe ESM: " + informeEspecialistaEnESM.getInformeElectronica() + "\n \t" +
				"Informe EPM: " + informeEspecialistaEnEPM.getInformeElectronica() ;
		return cadena;
	}

	public void setInformeEspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartamentNom() {
		return departamentNom;
	}

	public void setDepartamentNom(String departamentNom) {
		this.departamentNom = departamentNom;
	}



	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		super();
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return informeEspecialistaEnECM.getInformeElectronica();
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "EspecialistaEnECM: agafarTarees(): arreglar la electrònica d'Infermeria";
	}

	public String ferManteniment() {
		return "EspecialistaEnECM: ferManteniment(): ";
	}

}
