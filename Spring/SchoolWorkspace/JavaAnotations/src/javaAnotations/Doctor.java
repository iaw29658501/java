package javaAnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
// Nombre del id ( especificado pero esta id seria la por defecto)
@Component("doctorNau")
@Scope("singleton")
public class Doctor implements Tripulants {
	
	private InformeInfermeriaInterface informeDoctor;

	@Override
	public String agafarTarees() {
		return "doctorNau: agafarTarees(): curar als tripulants.";
	}

	@Override
	public String agafarInforme() {
		return "doctorNau: agafatInforme(): Informe del doctor de la missió " + this.getInformeDoctor().getInformeInfermeria();
	}

	@Autowired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		super();
		this.informeDoctor = informeDoctor;
	}

	public InformeInfermeriaInterface getInformeDoctor() {
		return informeDoctor;
	}

	public void setInformeDoctor(InformeInfermeriaInterface informeDoctor) {
		this.informeDoctor = informeDoctor;
	}

		
}
