package javaAnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class mainClassConfig {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		System.out.println("Després de carregar el context i abans de carregar el beans");
		Tripulants especialistaenecm = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println(especialistaenecm.agafarInforme());
		System.out.println(especialistaenecm.agafarTarees());
		
		System.out.println("-----------------------------------");
		EspecialistaEnECM especialista2 = contexte.getBean("especialistaEnECMTripleInforme", EspecialistaEnECM.class);
		System.out.println(especialista2.imprimirTripleInforme());
		System.out.println(especialista2.agafarTarees());
		System.out.println(especialista2.getEmail());
		System.out.println(especialista2.getDepartamentNom());
		contexte.close();
	}
}
