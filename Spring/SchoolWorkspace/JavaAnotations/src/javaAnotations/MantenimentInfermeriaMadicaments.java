package javaAnotations;

import org.springframework.stereotype.Component;

@Component("mantInferMedica")
public class MantenimentInfermeriaMadicaments implements MantenimentInfermeriaInterface {

	@Override
	public String ferMantenimentInfermeria() {
		return "ferMantenimentInfermeria(): manteniment dels medicaments de l'infermeria";
	}
	
}
