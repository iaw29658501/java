package javaAnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("infermeraNau")
@Scope("prototype")

public class Infermera implements Tripulants {
	// PROPRIEDADES
	@Autowired
	private InformeInfermeriaInterface informeInfermera;
	
	@Autowired
	@Qualifier("mantenimentInfermeriaMaterial") // Asi indicamos cual de las classes desta interface con que se hara la inyeccion
	private MantenimentInfermeriaInterface mantenimentMaterial;
	@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	
	public InformeInfermeriaInterface getInformeInfermera() {
		return informeInfermera;
	}
	
	@Autowired
	public void setInformeInfermera(InformeInfermeriaInterface informeInfermera) {
		this.informeInfermera = informeInfermera;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "infermeraNau: agafarTarees(): ajudar a curar als tripulants." ;
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "infermeraNau: agafarInforme(): Informe de l'infermera de la missió. " + informeInfermera.getInformeInfermeria();
	}

}
