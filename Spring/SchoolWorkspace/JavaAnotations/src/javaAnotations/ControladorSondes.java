package javaAnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class ControladorSondes implements Tripulants {

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "Tareas agafadas";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "informe agafat";
	}
	
	public void ControladorSonder() {
	}

	@PostConstruct
	public void inicialitzador() {
		System.out.println("ControladorSonder: Executado con JAVA ANNOTATION @PostConstruct");
	}
	
	@PreDestroy
	public void finalitzador() {
		System.out.println("ControladorSonder: Executado con JAVA ANNOTATION @PreDestroy");
	}
}
