package injeccion_spring;

import java.time.LocalDate;
import java.util.ArrayList;

public class Ruta {
	private int id;
	private String nom;
	private ArrayList<Waypoint> llistaWaypoints;
	private LocalDate dataCreacio;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public ArrayList<Waypoint> getLlistaWaypoints() {
		return llistaWaypoints;
	}
	public void setLlistaWaypoints(ArrayList<Waypoint> llistaWaypoints) {
		this.llistaWaypoints = llistaWaypoints;
	}
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	
	
}
