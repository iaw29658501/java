package injeccion_spring;

import java.time.LocalDate;

public class Waypoint {
	private int id;
	private String nom;
	private LocalDate dataCreacio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	
	
}
