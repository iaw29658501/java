package parte2;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component("departamentTipus2")

public class Departament {
	@Value("${empresaID}")
	int id;
	@Value("${empresaNom}")
	String nom;
	@Value("${empresaEmail}")
	String email;
	Organitzacio organization;
	Personal capDeDepartament;
	ArrayList<Personal> llistaPersonal;
	InformeDepartament informe;

	public Departament(int id, String nom, String email, Organitzacio organization, Personal capDeDepartament,
			ArrayList<Personal> llistaPersonal, InformeDepartament informe) {
		super();
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.organization = organization;
		this.capDeDepartament = capDeDepartament;
		this.llistaPersonal = llistaPersonal;
		this.informe = informe;
	}

	public Departament() {
		super();
	}

	@Override
	public String toString() {
		return "Departament [id=" + id + ", nom=" + nom + ", email=" + email + ", organization=" + organization
				+ ", capDeDepartament=" + capDeDepartament + ", llistaPersonal=" + llistaPersonal + ", informe="
				+ informe + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Organitzacio getOrganization() {
		return organization;
	}

	@Autowired
	@Qualifier("org")
	public void setOrganization(Organitzacio organization) {
		this.organization = organization;
	}

	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}

	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}

	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}

	public InformeDepartament getInforme() {
		return informe;
	}

	public void setInforme(InformeDepartament informe) {
		this.informe = informe;
	}

}
