package parte2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


public class Personal implements IPersonal {

	String dni;
	@Value("${capNom}")
	String nom;
	String cognom;
	String email;
	
	int numDepartament;
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getNumDepartament() {
		return numDepartament;
	}
	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}
	
}
