package parte2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ProtoTypeBean {
	private String param;		// En l'exemple d'Internet estava "final private String param;"
	
	public ProtoTypeBean(final String p) {
		this.param = p;
	}

	public String getParam() {
		return param;
	}

}
