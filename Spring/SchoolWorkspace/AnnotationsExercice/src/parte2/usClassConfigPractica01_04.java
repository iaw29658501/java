package parte2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class usClassConfigPractica01_04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("------INICIO-------");
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext("aplicacioConfigClass.class");
		Organitzacio orgB = ctx.getBean("organitzacioB", Organitzacio.class);
		System.out.println(orgB);
		ctx.close();
	}
}
