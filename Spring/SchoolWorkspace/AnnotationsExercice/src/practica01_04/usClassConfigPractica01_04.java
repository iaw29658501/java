package practica01_04;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usClassConfigPractica01_04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext("aplicacioConfig.class");
		Departament dpt = ctx.getBean("departamentTipus1", Departament.class);

		for (Personal p : dpt.getLlistaPersonal()) {
			System.out.println(p.getNom());
		}
	}
}
