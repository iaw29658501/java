package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usPractica01_04 {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext_Practica01_04.xml");
		Departament dpt = ctx.getBean("departamentTipus1", Departament.class);

		for (Personal p : dpt.getLlistaPersonal()) {
			System.out.println(p.getNom());
		}
	}
}
