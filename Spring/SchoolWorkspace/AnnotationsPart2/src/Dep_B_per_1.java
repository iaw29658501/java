

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("dep_B_per_1")
public class Dep_B_per_1 implements IPersonal {
	@Value("${capNom}")
	String nom;        
	@Value("${capDNI}")
	String dni;                                  
	String cognom;                               
	String email;                                
	public String getNom() {                     
		return nom;                              
	}                                            
	public void setNom(String nom) {             
		this.nom = nom;                          
	}                                            
	public String getDni() {                     
		return dni;                              
	}                                            
	public void setDni(String dni) {             
		this.dni = dni;                          
	}                                            
	public String getCognom() {                  
		return cognom;                           
	}                                            
	public void setCognom(String cognom) {       
		this.cognom = cognom;                    
	}                                            
	public String getEmail() {                   
		return email;                            
	}                                            
	public void setEmail(String email) {         
		this.email = email;                      
	}                                            
	                                             
}
