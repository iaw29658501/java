
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("parte2")
@PropertySource("classpath:parte2/practica01_04Departaments.propietats")
public class aplicacioConfigClass {
	
	@Bean 
	public Departament departamentTipus2() {
		//ArrayList<Personal> list = new ArrayList<Personal>();
		//list.add(1);
		return new Departament();
	}
	
	@Bean
	public Organitzacio organitzacioB() {
		return new Organitzacio();
	}
	
	@Bean
	public Dep_B_per_1 dep_B_per_1(){
		return new Dep_B_per_1();
	}
	
	@Bean
	public Dep_B_per_2 dep_B_per_2() {
		return new Dep_B_per_2();
	}

	

	

	
}
