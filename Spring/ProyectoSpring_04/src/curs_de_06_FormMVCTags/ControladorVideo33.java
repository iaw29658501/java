package curs_de_06_FormMVCTags;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/second")
public class ControladorVideo33 {
	@RequestMapping("respostaDelFormulari_29")
	public String processarFormulari(HttpServletRequest request , Model model) {
		String nom = request.getParameter("tripulantNom_33");
		String resp = "Colision de rutas : Qui hi ha (video 33) ? " + nom + " és un tripulant de la CCCP Leonov";
		model.addAttribute("missatgePerRetornar_33", resp);
		return "formulariNomResposta";
	}
}
