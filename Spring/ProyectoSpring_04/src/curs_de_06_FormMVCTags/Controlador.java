package curs_de_06_FormMVCTags;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	// for '/'
	@RequestMapping
	public String mostrarVista1() {
		// returning the view file (without the .jsp)
		return "vistaExemple_1";
	}
}}
