package curs_de_06_FormMVCTags;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tripulant")
public class ControladorTripulant {

	@RequestMapping("mostrarFormulariAltaTripulant")
	public String mostrarFormulari(Model model) {
		
		Tripulant doctor = new Tripulant();
		model.addAttribute("el_doctor", doctor);
		return "formulariAltaTripulant";
	}
	
	@RequestMapping("/processarAltaTripulant")
	public String processarFormulari(@ModelAttribute("el_doctor") Tripulant elNouTripulant) {
		
		return "confirmacioAltaNouTripulant";
	}
	
}

