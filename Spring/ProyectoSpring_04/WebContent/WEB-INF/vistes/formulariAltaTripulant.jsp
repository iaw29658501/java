<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<form:form action="processarAltaTripulant" modelAttribute="el_doctor">
	Nom el tripulant: <form:input path="nom"/>
	<br>
	Cognom : <form:input path="cognom"/>
	<br>
	Departament (nomes 1) <form:select path="departament">
							<form:option value="infermeria">Infermeria</form:option>
							<form:option value="maquines">Maquines</form:option>
							<form:option value="pont">Pont</form:option>
							</form:select>
<input type="submit" value="Donar d'alta"/>
</form:form>

</body>
</html>