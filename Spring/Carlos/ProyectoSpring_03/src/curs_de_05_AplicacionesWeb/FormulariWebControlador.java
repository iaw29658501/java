package curs_de_05_AplicacionesWeb;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormulariWebControlador {
	
	@RequestMapping("/mostraFormulari")
	public String mostraFormulari() {
		
		return "formulariNom";
		
	}
	
	@RequestMapping("/respostaDelFormulari_28")
	public String respostaDelFormulari() {
		
		return "formulariNomResposta";
		
	}
	
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {
		String nomTripulant = request.getParameter("tripulantNom_29");
		String resposta = "Qui hi ha? " + nomTripulant;
		model.addAttribute("missatgePerRetornar_29", resposta);
		return "formulariNomResposta";
		
	}
}
