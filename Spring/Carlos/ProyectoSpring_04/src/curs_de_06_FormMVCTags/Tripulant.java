package curs_de_06_FormMVCTags;

public class Tripulant {
	private String nom;
	private String cognom;
	private String departament;
	private String coneixements;
	private String ciutatNaixement;
	private String idiomes;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public String getConeixements() {
		return coneixements;
	}
	public void setConeixements(String coneixements) {
		this.coneixements = coneixements;
	}
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	
}
