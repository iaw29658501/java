package curs_de_06_FormMVCTags;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dirSegon")
public class ControladorVideo33 {
	
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {
		String nomTripulant = request.getParameter("tripulantNom_33");
		nomTripulant = nomTripulant + " és un tripulant de la CCCP Leonov";
		String resposta = "ALERTA DE COL·LISIÓ DE RUTES, "+nomTripulant;
		
		model.addAttribute("missatgePerRetornar_33", resposta);
		return "formulariNomResposta";
	}
}
