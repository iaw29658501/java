package curs_de_06_FormMVCTags;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	
	@RequestMapping
	public String mostrarVista1() {
		return "vistaExemple_1";
	}
}
