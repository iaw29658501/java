package curs_de_06_FormMVCTags;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormulariWebControlador {

	@RequestMapping("/mostrarFormulari")
	public String mostrarFormulari() {
		return "formulariNom";
	}
	
	@RequestMapping("/respostaDelFormulari_28")
	public String respostaDelFormulari() {
		return "formulariNomResposta";
	}
	
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {
		String nomTripulant = request.getParameter("tripulantNom_29");
		String resposta = " Qui hi ha (video 29)? " + nomTripulant + " és un tripulant de la CCCP Leonov";
		model.addAttribute("missatgePerRetornar_29",resposta);
		return "formulariNomResposta";
	}
	
	@RequestMapping("/respostaDelFormulari_32")
	public String procesarFormulari(@RequestParam("tripulantNom_32") String nomTripulant, Model model) {
		nomTripulant = nomTripulant + " és un tripulant de la CCCP Leonov";
		
		String resposta = "Qui hi ha (video 32)?"+nomTripulant;
		
		model.addAttribute("missatgePerRetornar_32",resposta);
		
		return "formulariNomResposta";
	}
}
