<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulari d'alta d'un tripulant . CCCP Leonov</title>
</head>
<body>
	<form:form action="procesarAltaTripulant" modelAttribute="el_doctor">
		Nom del tripulant: <form:input path="nom" />
		
		<br />
		<br />
		Cognom del tripulant: <form:input path="cognom" />
		<br />
		<br />
		Departament (només 1): <form:select path="departament">
			<form:option value="infermeria">Infermeria</form:option>
			<form:option value="maquines">Màquines</form:option>
			<form:option value="pont" label="Pont" />
		</form:select>
		<br />
		<br />
		Coneixements (sel·lecció multiple): <form:select path="coneixements" multiple="true">
			<form:option value="biologia" label="Biologia" />
			<form:option value="quimica" label="Quimica" />
			<form:option value="antropologia" label="Antropologia" />
			<form:option value="biologiaMolecular" label="Biologia molecular" />
		</form:select>
		<br />
		<br />
		Seleccionar una ciutat d'origen:
			<form:radiobutton path="ciutatNaixement" value="Barcelona" label="Barcelona" />
			<form:radiobutton path="ciutatNaixement" value="Estocolm" label="Estocolm" />
			<form:radiobutton path="ciutatNaixement" value="Helsinki" label="Helsinki" />
			<form:radiobutton path="ciutatNaixement" value="Oslo" label="Oslo" />
		<br />
		<br />
		Seleccionar els idiomes que sap:
			<form:checkbox path="idiomes" value="castella" label="Castellà"/>
			<form:checkbox path="idiomes" value="catala" label="Català"/>
			<form:checkbox path="idiomes" value="finlandes" label="Finlandès"/>
			<form:checkbox path="idiomes" value="suec" label="Suec"/>
		<br />
		<br />
		<input type="submit" value="Donar d'alta" />
	</form:form>

</body>
</html>