<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmació d'alta d'un nou tripulant - CCCP Leonov</title>
</head>
<body>
	S'ha donat d'alta el tripulant <b>${el_doctor.nom} ${el_doctor.cognom}</b>
	<br />
	<br />
	El departament del nou tripulant és <b>${el_doctor.departament}</b>
	<br />
	<br />
	Els coneixements del nou tripulant són <b>${el_doctor.coneixements}</b>
	<br />
	<br />
	La ciutat de naixement del nou tripulant és <b>${el_doctor.ciutatNaixement}</b>
	<br />
	<br />
	Els idiomes que sap el nou tripulant són <b>${el_doctor.idiomes}<br />
</body>
</html>