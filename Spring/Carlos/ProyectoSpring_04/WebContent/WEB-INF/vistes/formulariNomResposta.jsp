<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>formulariNomResposta.jsp</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
</head>
<body>
	Video 28 (omplert el formulari del video 28): <br>
	Aquesta és la resposta que envia el server al formulari que ha enviat l'usuari. <br>
	El nom del tripulant enviat per l'usuari a través del formulari ha estat: <b>${param.tripulantNom_28}</b><br>

	Video 29 (omplert el formulari del video 29): <br>
	<b>missatgePerRetornar:</b>${missatgePerRetornar_29}
	
	<br><br>
	-----------
	<br><br>
	
	Video 32 (omplert el formulari del video 32): <br>
	<b>missatgePerRetornar:</b>${missatgePerRetornar_32}
	<br><br>
	-----------
	<br><br>
	
	Video 33 (omplert el formulari del video 33): <br>
	<b>missatgePerRetornar:</b>${missatgePerRetornar_33}
	<br><br>
	-----------
	<br><br>
	<img alt="imagen" src="${pageContext.request.contextPath}/recursos/imatges/mia.jpg">
</body>
</html>