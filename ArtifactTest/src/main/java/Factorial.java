
public class Factorial {
	public static int factorial(int x) {
		if ( x == 0) {
			return 1;
		}
		// Error 
		if ( x < 0 ) {
			return -1;
		}
		int i, fact = 1;
		for (i = 1; i <= x; i++) {
			fact = fact * i;
		}
		return fact;
	}
}
