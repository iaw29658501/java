import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class TestFactorial {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//instancia = new Factorial();
	}

	@Test
	public void test() {
		assertEquals(1, Factorial.factorial(0));
	}
	
	@Test
	public void test2() {
		assertEquals(1, Factorial.factorial(1));
	}
	@Test
	public void test3() {
		assertEquals(6, Factorial.factorial(3));
	}
	
}
