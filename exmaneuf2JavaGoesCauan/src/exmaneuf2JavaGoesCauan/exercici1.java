package exmaneuf2JavaGoesCauan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class exercici1 {
	static File index = new File("fitxers_examen_2019_2020/exercici1/patata/index.txt");
	static File indexRoot = new File("fitxers_examen_2019_2020/index.txt");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File dir = new File("fitxers_examen_2019_2020/exercici1/patata");
		// File index = new File("fitxers_examen_2019_2020/exercici1/patata/index.txt");
		try {
			FileUtils.forceMkdir(dir);
			// FileUtils.touch(index);
			index.createNewFile();
			System.out.println("El fitchero index.txt existe en patata?" + index.exists());
			if (index.exists()) {
				FileUtils.write(index, "primera linea\n", "UTF-8");
				FileUtils.write(index, "Segunda linea", "UTF-8", true);
			}
			FileUtils.copyDirectoryToDirectory(new File("fitxers_examen_2019_2020/dirACopiar/"), dir);
			// 6
			FileUtils.copyFileToDirectory(index, new File("fitxers_examen_2019_2020/"));
			// 7 y 8
			Collection<File> listado = FileUtils.listFilesAndDirs(dir, TrueFileFilter.TRUE, TrueFileFilter.TRUE);
			for (File file : listado) {
				System.out.println("Listando propriedades de:");
				System.out.println(file.getAbsolutePath());
				System.out.println("se puede ler ?" + file.canRead());
				System.out.println("se puede escrever ?" + file.canWrite());
				System.out.println("se puede executar ?" + file.canExecute());
				System.out.println("esta oculto ?" + file.isHidden());
				System.out.println("cual su tamaño ?" + file.length());
			}
			// 9
			for (File file : listado) {
				copiarNomEnFitxer("Listando propriedades de:");
				copiarNomEnFitxer(file.getAbsolutePath());
				copiarNomEnFitxer("se puede ler ?" + file.canRead());
				copiarNomEnFitxer("se puede escrever ?" + file.canWrite());
				copiarNomEnFitxer("se puede executar ?" + file.canExecute());
				copiarNomEnFitxer("esta oculto ?" + file.isHidden());
				copiarNomEnFitxer("cual su tamaño ?" + file.length());
			}
			// 10
			FileUtils.cleanDirectory(dir);
			// 11

			File patatas = new File("fitxers_examen_2019_2020/index.txt");
			FileUtils.copyFileToDirectory(patatas, dir, true);
			System.out.println("Done");
			// 12
			System.out.println("Los index.txt son inguales ? " + FileUtils.contentEquals(index, indexRoot));

			
		} catch (FileNotFoundException e) {
			e.getMessage();
		} catch (IOException e) {
			e.getMessage();
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public static void copiarNomEnFitxer(String text) {
		try {
			FileUtils.write(indexRoot, text + "\n", "UTF-8", true);
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.getMessage();
		} catch (IOException e) {
			e.getMessage();
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
