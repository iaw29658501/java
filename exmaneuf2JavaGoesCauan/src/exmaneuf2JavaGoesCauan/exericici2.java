package exmaneuf2JavaGoesCauan;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class exericici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//////// Segunda Parte //////////
		try {
			File fichero = new File("fitxers_examen_2019_2020/fitxerProperties.txt");
			Properties prop = new Properties();
			FileInputStream inputStream = new FileInputStream(fichero);

			prop.load(inputStream);
			System.out.println(prop.getProperty("Class_Name"));
			inputStream.close();
			FileOutputStream output = new FileOutputStream(fichero);
			prop.setProperty("Overall_Strength_Index", "227_2");
			System.out.println("Class_Name as key in properties ? " + prop.containsKey("Class_Name"));
			System.out.println("Value 50 in properties ? " + prop.containsValue(50));
			System.out.println("Value 'approximately 30 - 50' in properties ? " + prop.containsValue("approximately 30 - 50"));
			System.out.println("Value '227' in properties ? " + prop.containsValue(227));
			System.out.println("Value '227_2' in properties ? " + prop.containsValue("227_2"));
			prop.store(output, null);
			output.close();
		} catch (FileNotFoundException e) {
			e.getMessage();
		} catch (IOException e) {
			e.getMessage();
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
