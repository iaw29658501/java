/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_2019_2020_DAW;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author gmartinez
 */
public class Ruta_Dades {
    int id;                             //Clau primaria. Es crea automàticament pel sistema i és intocable.
    String nom;
    ArrayList<Integer> llistaNumObjectius;            //Conté una llista dels ID's dels objectius que formen part de la ruta.
    ArrayList<Objectiu_Dades> llistaObjObjectius;    //Conté una llista dels objectes de tipus Objectiu que formen part de la ruta.
    boolean actiu;                      //TRUE si està activa.
    LocalDate dataCreacio;
    LocalDate dataModificacio;


    public ArrayList<Objectiu_Dades> getLlistaObjObjectius() {
        return llistaObjObjectius;
    }

    public Ruta_Dades(int id, String nom, ArrayList<Integer> llistaNumObjectius, ArrayList<Objectiu_Dades> llistaObjObjectius, boolean actiu, LocalDate dataCreacio, LocalDate dataModificacio) {
        this.id = id;
        this.nom = nom;
        this.llistaNumObjectius = llistaNumObjectius;
        this.llistaObjObjectius = llistaObjObjectius;
        this.actiu = actiu;
        this.dataCreacio = dataCreacio;
        this.dataModificacio = dataModificacio;
    }


    public LocalDate getDataModificacio() {
        return dataModificacio;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }


    public boolean isActiu() {
        return actiu;
    }

    public LocalDate getDataCreacio() {
        return dataCreacio;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public void setActiu(boolean actiu) {
        this.actiu = actiu;
    }

    public void setDataCreacio(LocalDate dataCreacio) {
        this.dataCreacio = dataCreacio;
    }


    public void setDataModificacio(LocalDate dataModificacio) {
        this.dataModificacio = dataModificacio;
    }


    @Override
    public String toString() {
        String aRetornar;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");

        aRetornar = "RUTA " + id + ":" +
                "\n   nom = " + nom +
                "\n   actiu = " + actiu +
                "\n   dataCreacio = " + dataCreacio.format(formatter) +
                "\n   dataModificació = " + dataModificacio.format(formatter) +
                "\n   llistaNumObjectius = " + llistaNumObjectius;

        aRetornar = aRetornar + "\n\n   llistaObjObjectius: ";
        aRetornar = aRetornar + "\n   -------";
        for (Objectiu_Dades ObjectiuTmp : llistaObjObjectius) {
            aRetornar = aRetornar + "\n   " + ObjectiuTmp.toString();
            aRetornar = aRetornar + "\n   -------";
        }

        return aRetornar;
    }

}
