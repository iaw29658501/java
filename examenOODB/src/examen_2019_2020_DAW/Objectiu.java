/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_2019_2020_DAW;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * @author gmartinez
 */
public class Objectiu {
    private static String nomBD = "baseDeDades/Leonov_2019_2020_DAW.db4o";

    //1. Carregar en memòria els objectius.
    public static ArrayList<Objectiu_Dades> menu1() {
        int[] coordenadesTmp = null;
        LocalDate today;
        LocalDate dataTmp1;
        LocalDate dataTmp2;
        LocalDate dataTmp3;
        LocalDate dataTmp4;
        ArrayList<Objectiu_Dades> llistaObjectiusTmp = new ArrayList<Objectiu_Dades>();


        today = LocalDate.now();
        dataTmp1 = LocalDate.of(2001, Month.JANUARY, 1);
        dataTmp2 = LocalDate.of(2005, Month.JUNE, 5);
        dataTmp3 = LocalDate.of(2010, Month.DECEMBER, 10);
        dataTmp4 = LocalDate.of(2030, Month.DECEMBER, 31);

        llistaObjectiusTmp.add(new Objectiu_Dades(0, "Òrbita de la Terra", true, dataTmp1, dataTmp1));

        llistaObjectiusTmp.add(new Objectiu_Dades(1, "Punt Lagrange Terra-LLuna", true, dataTmp2, dataTmp2));

        llistaObjectiusTmp.add(new Objectiu_Dades(2, "Òrbita de la LLuna", true, dataTmp3, dataTmp3));

        llistaObjectiusTmp.add(new Objectiu_Dades(3, "Òrbita de Mart", true, dataTmp4, dataTmp4));

        llistaObjectiusTmp.add(new Objectiu_Dades(4, "Òrbita de Júpiter", true, today, today));

        llistaObjectiusTmp.add(new Objectiu_Dades(5, "Punt Lagrange Júpiter-Europa", true, today, today));

        llistaObjectiusTmp.add(new Objectiu_Dades(6, "Òrbita de Europa", true, today, today));

        llistaObjectiusTmp.add(new Objectiu_Dades(7, "Òrbita de Venus", true, today, today));

        System.out.println("menu1(): CARREGAT EN LA MEMÒRIA RAM ELS OBJECTIUS.");

        return llistaObjectiusTmp;
    }


    public static void menu2(ArrayList<Objectiu_Dades> llistaObjectius) {
        // TODO Auto-generated method stub
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        try {
            for (Objectiu_Dades objectiu : llistaObjectius) {
                // Mirare si alguna nave con este ID ya existe en el banco de dados, si si la actualizo y si no la pongo
                try {
                    Predicate p = new Predicate<Objectiu_Dades>() {
                        @Override
                        public boolean match(Objectiu_Dades c) {
                            return objectiu.getId() == c.getId();
                        }
                    };

                    ObjectSet<Objectiu_Dades> result = db.query(p);
                    // si ya existe la borro
                    if (result.size() > 0) {
                        System.out.println("menu2(): UPDATE objectiu.Id = " + objectiu.getId() + ", objectiu.nom = " + objectiu.getNom());
                        db.delete(result.next());
                        db.store(objectiu);
                    } else {
                        System.out.println("menu2(): INSERT objectiu.Id = " + objectiu.getId() + ", objectiu.nom = " + objectiu.getNom());
                        db.store(objectiu);
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            System.out.println("menu2(): FINAL");
            db.close();
        }
    }

    public static void menu10() {
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        try {
            Predicate p = new Predicate<Objectiu_Dades>() {
                @Override
                public boolean match(Objectiu_Dades c) {
                    return true;
                }
            };

            ObjectSet<Objectiu_Dades> result = db.query(p);

            while (result.hasNext()) {
                Objectiu_Dades objectiu = result.next();
                System.out.println(objectiu.toString());
            }
        } finally {
            db.close();
            System.out.println("menu10(): FINAL");
        }
    }

    public static void menu11() {
        menu10();
        System.out.println("Sel·leciona un objectiu (Id) : ");
        Scanner in = new Scanner(System.in);
        int inputid = in.nextInt();
        in.nextLine();
        // VAMOS A VER SI ESTE ID DE NAVE REALMENTE EXISTE
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        try {
            Predicate p = new Predicate<Objectiu_Dades>() {
                @Override
                public boolean match(Objectiu_Dades c) {
                    return c.getId() == inputid;
                }
            };

            ObjectSet<Objectiu_Dades> result = db.query(p);
            // Miro si este id dado por el usuario no existe en la base de datos y si no existe lo digo
            if (result.size() == 0) {
                System.out.println("    NO S'HA TROBAT L'OBJECTIU " + inputid + " EN LA BD");
            } else {
                // EN ESTA CONDICION: Esta nave ya existe y tiene de ser actualizada
                System.out.println("INTRODUEIX LES NOVES DADES:");
                System.out.print("    Nom: ");
                String nombre = in.nextLine();
                Objectiu_Dades o = result.next();
                o.setNom(nombre);
                String actiu = "Waiting for input";

                while (!actiu.equalsIgnoreCase("n") && !actiu.equalsIgnoreCase("s")) {
                    System.out.print("    Actiu (S/N): ");
                    actiu = in.nextLine();
                }
                boolean act;
                if (actiu.equalsIgnoreCase("n")) {
                    act = false;
                } else {
                    act = true;
                }
                o.setActiu(act);
                db.store(o);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("menu11(): FINAL");
            db.close();
        }
    }

    public static void menu12() {
        System.out.println("1. En funció del ID");
        System.out.println("2. Alfabèticament");
        System.out.println("3. Per data de modificació (descendent)");
        Scanner sc = new Scanner(System.in);
        int order = sc.nextInt();
        sc.nextLine();
        ArrayList<Objectiu_Dades> arr = new ArrayList<Objectiu_Dades>();
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);

        try {
            Predicate p = new Predicate<Objectiu_Dades>() {
                @Override
                public boolean match(Objectiu_Dades c) {
                    return true;
                }
            };
            ObjectSet<Objectiu_Dades> result = db.query(p);
            while (result.hasNext()) {
                Objectiu_Dades cli = result.next();
                arr.add(cli);
            }
        } finally {
            db.close();
        }
        Comparator<Objectiu_Dades> comp = null;

        if (order == 3) {
            comp = new Comparator<Objectiu_Dades>() {
                @Override
                public int compare(Objectiu_Dades obj1, Objectiu_Dades obj2) {
                    if (obj1.getDataModificacio().isAfter(obj2.getDataModificacio())) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            };
            Collections.sort(arr, comp);
        } else if (order == 1 ) {
            comp = new Comparator<Objectiu_Dades>() {
                @Override
                public int compare(Objectiu_Dades obj1, Objectiu_Dades obj2) {
                    if (obj1.getId() > obj2.getId()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            };
            Collections.sort(arr, comp);
        } else {
            comp = new Comparator<Objectiu_Dades>() {
                @Override
                public int compare(Objectiu_Dades obj1, Objectiu_Dades obj2) {
                    return obj1.getNom().toLowerCase().compareTo(obj2.getNom().toLowerCase());
                }
            };
            Collections.sort(arr, comp);
        }

        for (Objectiu_Dades obj : arr) {
            System.out.println(obj.toString());
        }

        System.out.println("menu12(): FINAL");
    }
}