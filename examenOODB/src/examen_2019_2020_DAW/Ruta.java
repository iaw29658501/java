package examen_2019_2020_DAW;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.Scanner;

public class Ruta {

    private static String nomBD = "baseDeDades/Leonov_2019_2020_DAW.db4o";


    public static void menu20() {
        llistarObjectius();
        Ruta_Dades objectiu = novaRuta();
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        try {
            db.store(objectiu);
        } finally {
            db.close();
            System.out.println("menu20(): FINAL");
        }
    }

    public static int trobarNouIDRuta() {
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        int id = -1;
        try {
            Predicate p = new Predicate<Ruta_Dades>() {
                @Override
                public boolean match(Ruta_Dades c) {
                    return true;
                }
            };
            ObjectSet<Ruta_Dades> result = db.query(p);

            while (result.hasNext()) {
                Ruta_Dades ruta = result.next();
                if (ruta.getId() > id) {
                    id = ruta.getId();
                }
            }
        } finally {
            db.close();
        }
        return id + 1;
    }

    public static void llistarObjectius() {
        Objectiu.menu10();
    }

    public static Objectiu_Dades trobarObjectiuEnLaDB(int id) {
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        Objectiu_Dades objectiu = null;
        try {
            Predicate p = new Predicate<Objectiu_Dades>() {
                @Override
                public boolean match(Objectiu_Dades c) {
                    return c.getId() == id;
                }
            };
            ObjectSet<Objectiu_Dades> result = db.query(p);

            if (result.hasNext()) {
                objectiu = result.next();
            }
        } finally {
            db.close();
        }
        return objectiu;
    }

    public static Ruta_Dades novaRuta() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nombre: ");
        String nombre = sc.nextLine();
        System.out.println("Lista de ids (separados por espacios): ");

        String[] unfilterInput = sc.nextLine().split(" ");
        ArrayList<Integer> llistaNumObjectius = new ArrayList<Integer>();
        for (String i : unfilterInput) {
            llistaNumObjectius.add(Integer.parseInt(i));
        }

        String actiu = "Waiting for input";
        // Pregunto hasta que el usuario me de un S o N
        while (!actiu.equalsIgnoreCase("n") && !actiu.equalsIgnoreCase("s")) {
            System.out.print("    Actiu (S/N): ");
            actiu = sc.nextLine();
        }
        boolean activo;
        if (actiu.equalsIgnoreCase("n")) {
            activo = false;
        } else {
            activo = true;
        }
        LocalDate dataCreacion = LocalDate.now();
        ArrayList<Objectiu_Dades> llistaObjObjectius = new ArrayList<>();

        for (Integer i : llistaNumObjectius) {
            Objectiu_Dades o = trobarObjectiuEnLaDB(i);
            if (o != null) {
                llistaObjObjectius.add(o);
            }
        }
        Ruta_Dades novaRuta = new Ruta_Dades(trobarNouIDRuta(), nombre, llistaNumObjectius, llistaObjObjectius, activo, dataCreacion, dataCreacion);
        return novaRuta;
    }

    public static void menu21() {
        ObjectContainer db = Db4oEmbedded.openFile(nomBD);
        try {
            Predicate p = new Predicate<Ruta_Dades>() {
                @Override
                public boolean match(Ruta_Dades c) {
                    return true;
                }
            };
            ObjectSet<Ruta_Dades> result = db.query(p);
            while (result.hasNext()) {
                Ruta_Dades ruta = result.next();
                System.out.println(ruta.toString());
            }
        } finally {
            db.close();
            System.out.println("menu21(): FINAL");
        }
    }

    public static void menu22_i_23(int cascade) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Quina ruta vols esborrar de la DB? ");
        int value = sc.nextInt();
        sc.nextLine();
        System.out.println("Estas segur d'esborrar la ruta " + value + " de la BD [S/N] ? ");
        String sure = sc.nextLine();
        if (sure.equalsIgnoreCase("s")) {
            ObjectContainer db = null;
            if (cascade == 1) {
                EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
                config.common().objectClass(Ruta_Dades.class).cascadeOnDelete(true);
                db = Db4oEmbedded.openFile( config, nomBD);
                System.out.println("Deleting on cascade");
            } else {
                db = Db4oEmbedded.openFile(nomBD);
            }
            try {
                Predicate p = new Predicate<Ruta_Dades>() {
                    @Override
                    public boolean match(Ruta_Dades c) {
                        return value == c.getId();
                    }
                };
                ObjectSet<Ruta_Dades> result = db.query(p);
                if (result.size() != 1) {
                    System.out.println("La ruta con este id no existe o esta duplicada");
                } else {
                    Ruta_Dades deleteme = result.next();
                    if (cascade == 1 ) {
                        ArrayList<Objectiu_Dades> llistaObjObjectius = deleteme.getLlistaObjObjectius();
                        for (Objectiu_Dades o : llistaObjObjectius) {
                            db.delete(o);
                        }
                    }
                    db.delete(deleteme);
                }
            } finally {
                db.close();
            }
        }
    }
}
