/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_2019_2020_DAW;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author gmartinez
 */
public class Krona {
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }


    public static void menuKrona() throws IOException  {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");

        ArrayList<Objectiu_Dades> llistaObjectius = new ArrayList<Objectiu_Dades>();


        do {
            menu.delete(0, menu.length());

            menu.append(System.getProperty("line.separator"));
            menu.append("RV-18A Krona ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            menu.append("1. Carregar en memòria els objectius");
            menu.append(System.getProperty("line.separator"));
            menu.append("2. Carregar en la BD els objectius carregats en memòria");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            menu.append("10. LListar els objectius de la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append("11. Modificar un objectiu de la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append("12. Ordena i visualitzar els objectius de la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            menu.append("20. Crear una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("21. LListar les rutes (amb tota la informació dels objectius)");
            menu.append(System.getProperty("line.separator"));
            menu.append("22. Esborrar de la BD una ruta però sense esborrar els objectius de la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append("23. Esborrar de la BD una ruta i els objectius que tingui assignats");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("50. Tornar al menú pare (PNS-24 Puma)");
            menu.append(System.getProperty("line.separator"));


            System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

            opcio = sc.next();

            switch (opcio) {
                case "1":
                    llistaObjectius = Objectiu.menu1();
                    bloquejarPantalla();
                    break;
                case "2":
                    Objectiu.menu2(llistaObjectius);
                    bloquejarPantalla();
                    break;
                case "10":
                    Objectiu.menu10();
                    bloquejarPantalla();
                    break;
                case "11":
                    Objectiu.menu11();
                    bloquejarPantalla();
                    break;
                case "12":
                    Objectiu.menu12();
                    bloquejarPantalla();
                    break;
                case "20":
                    Ruta.menu20();
                    bloquejarPantalla();
                    break;
                case "21":
                    Ruta.menu21();
                    bloquejarPantalla();
                    break;
                case "22":
                    Ruta.menu22_i_23(0);
                    bloquejarPantalla();
                    break;
                case "23":
                    Ruta.menu22_i_23(1);
                    bloquejarPantalla();
                    break;
                case "50":
                    break;
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }
        } while (!opcio.equals("50"));
    }

}
