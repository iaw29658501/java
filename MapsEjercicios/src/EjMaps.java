import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class EjMaps {
	
	static LinkedHashMap<String, Vehicle> m;
	
	public static void main(String[] args) {

		m = new LinkedHashMap<String, Vehicle>();
		m.put("B8240KY", new Vehicle("Blanco", "Opel", "kadet", 1990));
		m.put("8040BGZ", new Vehicle("Rojo", "Seat", "panda", 1985));
		m.put("8140BGZ", new Vehicle("Negro", "Seat", "leon", 2005));
		m.put("8240BGZ", new Vehicle("Azul", "Volkswagen", "passat", 2007));
		m.put("8140BGY", new Vehicle("Negro", "Seat2", "asdfg", 1999));
		System.out.println(m.toString());
		removeOld(m);
		System.out.println(m.toString());
		System.out.println("Cotxes negros:");
		System.out.println(selectColor(m , "Negro" ).toString());
		
		//marca = new LinkedHashMap<String, Vehicle>();
		
	}

	public static void removeOld(LinkedHashMap<String, Vehicle> cotxes) {
		for (Map.Entry<String, Vehicle> cotxe : cotxes.entrySet()) {
			if (cotxe.getValue().getAny() <= 1985) {
				// cotxes.remove(cotxe.getKey());
				System.out.println("Menos un");
			}
		}

	}

	public static ArrayList<Vehicle> selectColor(LinkedHashMap<String, Vehicle> cotxes, String color) {
		ArrayList<Vehicle> retArray = new ArrayList<Vehicle>();
		for (Map.Entry<String, Vehicle> cotxe : cotxes.entrySet()) {
			if (cotxe.getValue().getColor().equals(color)) {
				// cotxes.remove(cotxe.getKey());
				retArray.add((Vehicle) cotxe.getValue());
			}
		}

		return retArray;
	}

	public static void showMarca(LinkedHashMap<String,ArrayList<Vehicle>> map) {
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			System.out.println("Marca : " + key);
			System.out.println(map.get(key));
			System.out.println();
		}
	}
}
