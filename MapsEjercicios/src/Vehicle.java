
public class Vehicle {
	String color;
	String marca;
	String model;
	Integer any;
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Integer getAny() {
		return any;
	}
	public void setAny(Integer any) {
		this.any = any;
	}
	public Vehicle(String color, String marca, String model, Integer any) {
		super();
		this.color = color;
		this.marca = marca;
		this.model = model;
		this.any = any;
	}
	@Override
	public String toString() {
		return "Vehicle [color=" + color + ", marca=" + marca + ", model=" + model + ", any=" + any + "]";
	}
	
}
