import java.io.FileOutputStream;
import java.io.IOException;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        // declares an array of integers
		try {
			obrirFitxer();
		} catch (Exception e) {
			System.out.println("Bypass");
		}
	}

    private static void obrirFitxer() throws IOException 
    {
        //En throws l'opció FileNotFoundException no fa falta perque està dins
        //de la IOException.
        //FileOutputStream pot generar 2 errors: FileNotFoundException i 
        //SecurityException. SecurityException està dins de RuntimeException
        //i per això no fa falta ficar-la en el throws. Les RuntimeException i 
        //tot lo que hi ha a dins no s'ha de ficar en el throws.
        FileOutputStream f = new FileOutputStream ("/tmp/a.txt");
        f.close();
    }
    
    
    static void exercici2()
    {
        try {
                obrirFitxer();
        } catch (IOException e) {
            System.out.println("Codi del catch:");
            System.out.println("e.getMessage() = " + e.getMessage());
            System.out.println("e.getCause() = " + e.getCause());
            System.out.print("e.printStackTrace() = " );
            e.printStackTrace();
            System.out.println();
            System.out.println("e.toString() = " + e.toString());
            System.out.println("Fi de codi del catch.");
	}        
    } 

}
