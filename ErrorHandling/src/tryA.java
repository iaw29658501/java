import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class tryA {

	public static void main(String[] args) throws IOException {
		/*
		 * List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","",
		 * "jkl");
		 * 
		 * //get count of empty stringoo long count =
		 * strings.parallelStream().filter(string -> string.isEmpty()).count();
		 * System.out.println(count);
		 */
		File file1 = new File("/tmp/file1.txt");
		File file2 = new File("/tmp/file2.txt");
		// el segundo parametro 'true' es para el append
		FileWriter fw1 = new FileWriter(file1);
		PrintWriter pw1 = new PrintWriter(fw1, true);

		FileWriter fw2 = new FileWriter(file2);
		PrintWriter pw2 = new PrintWriter(fw2, true);

		FileReader fr1 = null;
		FileReader fr2 = null;
		String linea1;
		String linea2;

		try {
			int i = 0;
			while (i < 10) {
				pw1.println("Linea" + i++);
			}
			pw1.println("this is a different line");

			int j = 0;
			while (j < 11) {
				pw2.println("Linea" + j++);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			fw1.close();
			fw2.close();
		}

		try {
			fr1 = new FileReader(file1);
			fr2 = new FileReader(file2);
			BufferedReader br1 = new BufferedReader(fr1);
			BufferedReader br2 = new BufferedReader(fr2);
			int lineaActual = 0;
			while ((linea1 = br1.readLine()) != null && (linea2 = br2.readLine()) != null) {
				lineaActual++;
				if (! linea1.equals(linea2)) {
					System.out.println("Fitxero distinto en la linea " + lineaActual);
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("File1 not found " + e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null) {
				fr1.close();
			}
			if (fr2 != null) {
				fr2.close();
			}
		}
	}
}
