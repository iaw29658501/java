import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExRandomAccessFile {


	private static RandomAccessFile fitxerEmmagatzematge;

	public static void main(String [] args)
	{
		ArrayList<Naus_Dades> arrayListDeNaus = new ArrayList<Naus_Dades>();
        LocalDateTime today = LocalDateTime.now();    
        arrayListDeNaus.add(new Naus_Dades("Agamemnon", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Babylon 5"));
        arrayListDeNaus.add(new Naus_Dades("Achilles", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Babylon 5"));
        arrayListDeNaus.add(new Naus_Dades("Cortez", "explorador", "Explorer".toCharArray(), today, "Enviat a l'espai profund"));
        File fichero = new File("/tmp/nausfile.txt");
        try {
        	 fitxerEmmagatzematge = new RandomAccessFile(fichero, "rw");
        	 for(Naus_Dades nauTmp : arrayListDeNaus){
                 System.out.println(nauTmp.getDataConstruccio());
                 // Gravem el nom (String nom).
                 Integer longString = nauTmp.getNom().length();
                 fitxerEmmagatzematge.writeInt(longString);
                 fitxerEmmagatzematge.writeChars(nauTmp.getNom());
                 
                 // Gravem el tipus (String tipus).
                 longString = nauTmp.getTipus().length();
                 fitxerEmmagatzematge.writeInt(longString);
                 fitxerEmmagatzematge.writeChars(nauTmp.getTipus());                
                 
                 // Gravem el model (char model[50]).
                 // Per a passar de char[] --> String: String.valueOf(X) on X és de tipus char[].
                 StringBuffer buffer = new StringBuffer(String.valueOf(nauTmp.getModel()));
                 buffer.setLength(50);
                 fitxerEmmagatzematge.writeChars(buffer.toString());
                 
                 // Gravem la dataConstruccio (LocalDateTime dataConstruccio).
                 fitxerEmmagatzematge.writeChars(String.valueOf(nauTmp.getDataConstruccio().toString()));
                 
                 // Gravem la descripció (String descripcio).
                 longString = nauTmp.getDescripcio().length();
                 fitxerEmmagatzematge.writeInt(longString);
                 fitxerEmmagatzematge.writeChars(nauTmp.getDescripcio()); 
             }

             String stringTmp;
             fitxerEmmagatzematge.close();
             
             fitxerEmmagatzematge = new RandomAccessFile(fichero, "r");
             
             while (fitxerEmmagatzematge.getFilePointer() != fitxerEmmagatzematge.length()) {
             	System.out.println("----------------");
             	
 	            // LLegim el nom (String nom).
             	stringTmp = "";
 	            Integer longString = fitxerEmmagatzematge.readInt();
 	            System.out.println("Nom.length(): " + longString);
 	            for(int i = 0; i< longString; i++) {
 	                //System.out.println("Nom: " + fitxerEmmagatzematge.readChar());
 	            	stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
 	            }
 	            System.out.println("Nom: " + stringTmp);
 	            
 	            // LLegim el tipus (String tipus).
 	            stringTmp = "";
 	            longString = fitxerEmmagatzematge.readInt();
 	            System.out.println("tipus.length(): " + longString);
 	            for(int i = 0; i< longString; i++) {
 	                //System.out.println("Tipus: " + fitxerEmmagatzematge.readChar());
 	            	stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
 	            }
 	            System.out.println("Tipus: " + stringTmp);
 	            
 	            // LLegim el model (char model[50]).    
 	            stringTmp = "";
 	            for(int i = 0; i< 50; i++) {
 	                //System.out.println("Model: " + fitxerEmmagatzematge.readChar());
 	                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
 	            }
 	            System.out.println("Model: " + stringTmp);
 	            
 	            // LLegim la dataConstruccio (LocalDateTime dataConstruccio).
 	            stringTmp = "";
 	            for(int i = 0; i< 23; i++) {
 	                //System.out.println("Data construcció: " + fitxerEmmagatzematge.readChar());
 	                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
 	            }
 	            System.out.println("Data construcció: " + stringTmp);
 	            
 	            // LLegim la descripció (String descripcio).
 	            stringTmp = "";
 	            longString = fitxerEmmagatzematge.readInt();
 	            System.out.println("descripcio.length(): " + longString);
 	            for(int i = 0; i< longString; i++) {
 	                //System.out.println("Descripció: " + fitxerEmmagatzematge.readChar());
 	            	stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
 	            }
 	            System.out.println("Descripció: " + stringTmp);
             }
             
         } catch (FileNotFoundException ex) {
             ex.getMessage();
         } catch (IOException ex) {
        	 ex.getMessage();
         }
         
        
	}
}
