package people;

import java.util.List;

public class Contact implements Comparable<Contact>{
	private String name;
	private String surname;
	private String surname2;
	private String email;
	private String phone;
	private String cellphone;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSurname2() {
		return surname2;
	}
	public void setSurname2(String surname2) {
		this.surname2 = surname2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCellphone() {
		return cellphone;
	}
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	public Contact(String name, String surname, String surname2, String email, String phone, String cellphone) {
		super();
		this.name = name;
		this.surname = surname;
		this.surname2 = surname2;
		this.email = email;
		this.phone = phone;
		this.cellphone = cellphone;
	}
	public Contact(String name, String surname, String surname2) {
		super();
		this.name = name;
		this.surname = surname;
		this.surname2 = surname2;
	}
	
	@Override
	public int compareTo(Contact c){
		return this.getName().compareTo(c.getName());
	}
	static void print( List<Contact> a ) {
		for ( Contact c : a) {
			System.out.println(c.getName() + " " + c.getSurname() + " " + c.getSurname2());
		}
	}
	
	
}
