package people;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class mainAgenda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Contact a = new Contact( "David", "Muroña", "Uraño");
		Contact c = new Contact( "Diego", "Wagensberg", "Mateos");
		Contact d = new Contact( "Cauan", "Goes", "Mateos");
		Contact e = new Contact( "Pedro", "Soares" ,"Silva");
		
		List<Contact> agenda = new ArrayList<Contact>();
		
		agenda.add(a);
		agenda.add(c);
		agenda.add(d);
		agenda.add(e);
		
		Contact.print(agenda);
		System.out.println();

		Collections.sort(agenda);

		Contact.print(agenda);
		System.out.println();
		
		
		Collections.sort(agenda, new Comparator<Contact>() {
			@Override
			public int compare(Contact a, Contact b) {
				return a.getSurname().compareTo(b.getSurname());
			}
			
		});

		Contact.print(agenda);
		System.out.println();

		
	}

}
