
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaMundo
 */
@WebServlet(description = "testing", urlPatterns = { "/HolaMundo" })
public class HolaMundo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public HolaMundo() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		PrintWriter printwriter = response.getWriter();

		printwriter.println(docType);
		printwriter.println(" <form>\n" + "  First name:<br>\n" + "  <input type=\"text\" name=\"firstname\"><br>\n"
				+ " Surname name:<br>\n" + "  <input type=\"text\" name=\"surname\"><br>\n" + "  email <br>\n"
				+ "  <input type=\"email\" name=\"email\">\n" + "    <br> DNI:<br>\n"
				+ "  <input type=\"text\" name=\"dni\">\n" + "    <br> date of birth (dd/mm/yyyy):<br>\n"
				+ "  <input type=\"text\" name=\"date\">\n" + "<br> <button type=\"submit\">submit</button>"
				+ "</form> ");
		printwriter.println();

		ArrayList<String> errores = new ArrayList<String>();
		// Check name
		if (Pattern.matches("^[A-Z][a-z]{2,50}$", request.getParameter("firstname"))) {
			printwriter.println("Nice name<br>");
		} else {
			errores.add("<br>Invalido name please only use letters<br>");
		}

		if (Pattern.matches("^[A-Z\\sa-z]{3,60}$", request.getParameter("surname"))) {
			printwriter.println("Nice surname<br>");
		} else {
			errores.add("<br>Surname invalid<br>");
		}
		
		if (Pattern.matches("^[A-Za-z0-9._%+-]{1,100}@[A-Za-z0-9.-]{1,50}\\.[A-Za-z]{2,4}$", request.getParameter("email"))) {
			printwriter.println("Nice email<br>");
		} else {
			errores.add("<br>Invalid Email<br>");
		}
		
		if (Pattern.matches("^\\d\\d/\\d\\d/\\d\\d\\d\\d$", request.getParameter("date"))) {
			printwriter.println("Nice day of birth<br>");
		} else {
			errores.add("<br>Invalid birth day<br>");
		}
		
		
		// check dni
		if (Pattern.matches("^[0-9]{8,8}[A-Za-z]$", request.getParameter("dni"))) {
			String dni = request.getParameter("dni");
			String dnicheck = "TRWAGMYFPDXBNJZSQVHLCKE";
			int neededPosition = Integer.parseInt(dni.substring(0, dni.length() - 1)) % 23;
			if (dnicheck.charAt(neededPosition) == dni.charAt(dni.length()-1)) {
				printwriter.println("Nice dni<br>");
			}  else {
				errores.add("<br>Invalid dni<br>");
			}
		} else {
			errores.add("<br>Invalid dni<br>");
		}

		// output errors
		for ( String s: errores ) {
			printwriter.println(s);
		}
		
		
	}

}
