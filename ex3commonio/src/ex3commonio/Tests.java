package ex3commonio;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class Tests {

	public static void main(String[] args) throws IOException {
		File dir = new File("/tmp/a/b/c/");
		FileUtils.forceMkdir(dir);
		File createme = null;
		for ( int i = 1; i < 6; i++) {
			createme = new File("/tmp/fitxer" +i + ".txt");
			createme.createNewFile();
			FileUtils.moveFile(createme, new File( dir.getAbsolutePath() + "/" + createme.getName()));
		}
		System.out.println("Contains? " + FileUtils.directoryContains(dir, new File("/tmp/a/b/c/fitxer3.txt")));
		// 6
		try {
			FileUtils.forceDelete(new File("/tmp/a/b/c/fitxer3.txt"));
		} catch (Exception e) {
			e.getStackTrace();
		}
	
		System.out.println("Contains? " + FileUtils.directoryContains(dir, new File("/tmp/a/b/c/fitxer3.txt")));
	
		System.out.println("Is file older? " + FileUtils.isFileOlder(new File("/tmp/a/b/c/fitxer1.txt"),new File("/tmp/a/b/c/fitxer5.txt")));
		FileUtils.forceDelete(new File ("/tmp/a/"));
		
	}
}
