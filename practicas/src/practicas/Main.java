package practicas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// Linkedlist vs ArrayList
		LinkedList ll = new LinkedList();
		List lista = new ArrayList();
		// Insersio Massiva
		long tempsInicial = System.nanoTime();
		for (int i = 0; i < 10000000; i++) {
			lista.add(i);
		}
		long tempsFinal = System.nanoTime();
		System.out.println("10000000 obj: ArrayList a tardado " + ( tempsFinal - tempsInicial )/1000);
		
		long tempsInicial2 = System.nanoTime();
		for (int i = 0; i < 10000000; i++) {
			ll.add(i);
		}
		long tempsFinal2 = System.nanoTime();
		System.out.println("10000000 obj: LinkedList a tardado " + (tempsFinal2 - tempsInicial2)/1000);
		// Add por posicion
		System.out.println("Add por posicion");
		tempsInicial  = System.nanoTime();
		lista.add(0, 007);
		tempsFinal  = System.nanoTime();
		System.out.println("ArrayList a tardado " + ( tempsFinal - tempsInicial ));
		
		tempsInicial  = System.nanoTime();
		ll.addFirst(-1);
		tempsFinal  = System.nanoTime();
		System.out.println("LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		System.out.println("");
		
	
		
		tempsInicial  = System.nanoTime();
		lista.add(5000,1337);
		tempsFinal  = System.nanoTime();
		System.out.println("5.000 ArrayList a tardado " + ( tempsFinal - tempsInicial ));

		// Adding element to 5.000 position
		tempsInicial  = System.nanoTime();
		ll.add(5000, 1337);;
		tempsFinal  = System.nanoTime();
		System.out.println("5.000 LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		
		System.out.println("");
		
		
		tempsInicial  = System.nanoTime();
		lista.add(10000,1337);
		tempsFinal  = System.nanoTime();
		System.out.println("adding to [10.000] ArrayList a tardado " + ( tempsFinal - tempsInicial ));

		// Adding element to 5.000 position
		tempsInicial  = System.nanoTime();
		ll.add(10000, 1337);;
		tempsFinal  = System.nanoTime();
		System.out.println("adding to 10.000 LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		// Obteniendo valores
		
		System.out.println("");
		//0
		
		tempsInicial  = System.nanoTime();
		System.out.println(lista.get(0));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 0 ArrayList a tardado " + ( tempsFinal - tempsInicial ));

		tempsInicial  = System.nanoTime();
		System.out.println(ll.get(0));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 0 LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		System.out.println("");
		
		// 5000
		tempsInicial  = System.nanoTime();
		System.out.println(lista.get(5000));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 5000 ArrayList a tardado " + ( tempsFinal - tempsInicial ));


		tempsInicial  = System.nanoTime();
		System.out.println(ll.get(5000));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 5000 LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		// 10000
		tempsInicial  = System.nanoTime();
		System.out.println(lista.get(10000));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 10000 ArrayList a tardado " + ( tempsFinal - tempsInicial ));


		tempsInicial  = System.nanoTime();
		System.out.println(ll.get(10000));
		tempsFinal  = System.nanoTime();
		System.out.println("Lendo 10000 LinkedList a tardado " + ( tempsFinal - tempsInicial ));

		
		// Ex2 : Adicionando por posicion usaria LinkedList
		// 		 Eliminar un elemento por comparacion a otro: ????
		// 		 Acediremos a elementos por posicion absoluta usaria : ArrayList
		
		
		// Ex3 : 1.No la ArrayList pesa menos que el vector
		//		 2. Porque solo una thread (hilo de executcion) puede hacederlo por hora entonces no hace falta el iterador

		
	}

}
