package practicas;

import java.util.Calendar;
import java.util.List;

public class Contact implements Comparable<Contact>{
	private String name;
	private int number;
	private String email;
	private String cognom;
	private String segonCognom;
	private int mobil;
	
	
	public Contact(String name, String cognom, String segonCognom) {
		super();
		this.name = name;
		this.cognom = cognom;
		this.segonCognom = segonCognom;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSegonCognom() {
		return segonCognom;
	}
	public void setSegonCognom(String segonCognom) {
		this.segonCognom = segonCognom;
	}
	public int getMobil() {
		return mobil;
	}
	public void setMobil(int mobil) {
		this.mobil = mobil;
	}
	
	public static void imprimirAgenda(List<Contact> agenda) {
		for ( Contact c: agenda ) {
			System.out.println(c.getName() + " " + c.getCognom() + " " + c.getSegonCognom());
		}
	}
	
	@Override
	public int compareTo(Contact o) {
		// TODO Auto-generated method stub
		int result = this.getCognom().compareTo(o.getCognom());
		if (result == 0){
			result = this.getName().compareTo(o.getName());
		}
		return result;
	}
	
}
