package practicas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Iterator;

public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> numbs = new ArrayList<Integer>();

		for (String arg : args) {
			numbs.add(Integer.parseInt(arg));
		}
		System.out.println("Size : " + numbs.size());

		for (int i = 0; i < numbs.size(); i++) {
			numbs.set(i, (numbs.get(i) * 2));
		}

		for (int i = 0; i < numbs.size(); i++) {
			System.out.println(numbs.get(i));
		}

		Iterator<Integer> it = numbs.iterator();
		while (it.hasNext()) {
			if (it.next() > 100) {
				it.remove();
			}
		}
		Collections.sort(numbs);

		for (Integer numb : numbs) {
			System.out.println(numb);
		}

		// Exercicio 2
		List<Integer> numbs2 = new ArrayList<Integer>();
		numbs2.add(10);
		numbs2.add(20);
		numbs2.add(30);
		numbs.addAll(numbs2);
		System.out.println();

		for (Integer numb : numbs2) {
			System.out.println(numbs.contains(numb));
		}
		numbs2.clear();
		for (Integer numb : numbs2) {
			System.out.println(numb);
		}
		
		// Exercicio 6
				
		List<Person> agenda = new ArrayList<Person>();
		
		Person d = new Person("David", "Moraño", "Ureña");
		Person p = new Person("Pol", "Nerdy", "LoL");
		
		agenda.add(d);
		agenda.add(p);
		
		Collections.sort(agenda);
				
		
	}

}
