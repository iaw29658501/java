package wb;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class swt {

	protected Shell shell;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private Text text;
	private ScriptEngine engine;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			swt window = new swt();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		
		
		Display display = Display.getDefault();
		
	    this.engine = (new ScriptEngineManager()).getEngineByName("JavaScript");
		
		createContents();
		
		
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(557, 427);
		shell.setText("SWT Application");
		
		Button btn1 = formToolkit.createButton(shell, "1", SWT.NONE);
		btn1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "1" );
			}
		});
		btn1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btn1.setBounds(43, 89, 95, 34);
		
		Button btn3 = formToolkit.createButton(shell, "3", SWT.NONE);
		btn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "3" );
			}
		});
		btn3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btn3.setBounds(282, 90, 95, 34);
		
		Button btn6 = formToolkit.createButton(shell, "6", SWT.NONE);
		btn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "6" );
			}
		});
		btn6.setBounds(282, 141, 95, 34);
		
		Button btn8 = formToolkit.createButton(shell, "8", SWT.NONE);
		btn8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "8" );
			}
		});
		btn8.setBounds(157, 199, 95, 34);
		
		Button btn0 = formToolkit.createButton(shell, "0", SWT.NONE);
		btn0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "0" );
			}
		});
		btn0.setBounds(157, 254, 95, 34);
		
		Button btn2 = formToolkit.createButton(shell, "2", SWT.NONE);
		btn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "2" );
			}
		});
		btn2.setBounds(157, 89, 95, 34);
		
		Button btn4 = formToolkit.createButton(shell, "4", SWT.NONE);
		btn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "4" );
			}
		});
		btn4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btn4.setBounds(43, 144, 95, 34);
		
		Button btn5 = formToolkit.createButton(shell, "5", SWT.NONE);
		btn5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "5" );
			}
		});
		btn5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btn5.setBounds(157, 144, 95, 34);
		
		Button btn7 = formToolkit.createButton(shell, "7", SWT.NONE);
		btn7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "7" );
			}
		});
		btn7.setBounds(43, 199, 95, 34);
		
		Button btn9 = formToolkit.createButton(shell, "9", SWT.NONE);
		btn9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "9" );
			}
		});
		btn9.setBounds(282, 199, 95, 34);
		
		text = new Text(shell, SWT.BORDER);
		text.setBounds(43, 25, 469, 44);
		formToolkit.adapt(text, true, true);
		
		Button btnEqual = new Button(shell, SWT.NONE);
		btnEqual.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				String foo = text.getText();
				try {
					text.setText(engine.eval(foo).toString());
				} catch(Exception ex) {
					text.setText("ERROR");	
				}
			}
		});
		btnEqual.setBounds(282, 254, 95, 34);
		formToolkit.adapt(btnEqual, true, true);
		btnEqual.setText("=");
		
		Button btnClear = new Button(shell, SWT.NONE);
		btnClear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( "" );
			}
		});
		btnClear.setBounds(43, 254, 95, 34);
		formToolkit.adapt(btnClear, true, true);
		btnClear.setText("Clear");
		
		Button btnSum = new Button(shell, SWT.NONE);
		btnSum.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "+" );
			}
		});
		btnSum.setBounds(417, 90, 95, 34);
		formToolkit.adapt(btnSum, true, true);
		btnSum.setText("+");
		
		Button btnDot = new Button(shell, SWT.NONE);
		btnDot.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "." );
			}
		});
		btnDot.setBounds(43, 309, 95, 34);
		formToolkit.adapt(btnDot, true, true);
		btnDot.setText(".");
		
		Button btnMultiply = new Button(shell, SWT.NONE);
		btnMultiply.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "*" );
			}
		});
		btnMultiply.setBounds(417, 204, 95, 34);
		formToolkit.adapt(btnMultiply, true, true);
		btnMultiply.setText("*");
		
		Button btnSubtract = new Button(shell, SWT.NONE);
		btnSubtract.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "-" );
			}
		});
		btnSubtract.setBounds(417, 141, 95, 34);
		formToolkit.adapt(btnSubtract, true, true);
		btnSubtract.setText("-");
		
		Button btnDivide = new Button(shell, SWT.NONE);
		btnDivide.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setText( text.getText() + "/" );
			}
		});
		btnDivide.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnDivide.setBounds(417, 254, 95, 34);
		formToolkit.adapt(btnDivide, true, true);
		btnDivide.setText("/");

	}
}
